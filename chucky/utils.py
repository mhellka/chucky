import os
import threading
import time
import logging
import contextlib
import re
from pathlib import Path

log = logging.getLogger(__name__)


class FileLockFailed(IOError):
    pass


class FileLock:
    """An exclusive file-lock for inter-process locking.

    Also works as a re-entrant lock within the same process.
    """

    def __init__(self, lockfile):
        self.lockfile = Path(lockfile).absolute()
        self.fd = None
        self.tlock = threading.RLock()
        self.depth = 0

    @contextlib.contextmanager
    def __call__(self, timeout=-1, interval=0.1):
        self.acquire(timeout, interval)
        try:
            yield self
        finally:
            self.release()

    def __enter__(self):
        self.acquire()
        return self

    def __exit__(self, type, value, traceback):
        self.release()

    def is_locked(self):
        """Return true if this lock is currently held by someone"""
        return self.fd is not None or self.lockfile.exists()

    def wait(self, timeout=-1, interval=0.1):
        """Blocking-wait for this lock to be released without acquireing it."""
        deadline = time.time() + timeout if timeout > 0 else float("inf")
        while True:
            if not self.lockfile.exists():
                return True
            if time.time() + interval > deadline:
                return False
            time.sleep(interval)

    def acquire(self, timeout=-1, interval=0.1):
        """Same as try_acquire, but tries forever by default and throws on failure."""
        if not self.try_acquire(timeout, interval):
            raise FileLockFailed(f"Failed to acquire lock: {self.lockfile!s}")
        return self

    def try_acquire(self, timeout=0, interval=0.1):
        """Try to acquire the lock. Return true on success.

        If not possible, this method will sleep for interval seconds and try again until the timeout is reached.
        The default timeout is zero (try just once without blocking)."""

        self.tlock.acquire(blocking=True, timeout=timeout)
        self.depth += 1
        deadline = time.time() + timeout if timeout > 0 else float("inf")
        while True:
            if self.fd:
                return True

            try:
                self.lockfile.parent.mkdir(parents=True, exist_ok=True)
                self.fd = os.open(self.lockfile, os.O_CREAT | os.O_EXCL | os.O_WRONLY)
                os.write(self.fd, str(os.getpid()).encode("ASCII"))
                return True
            except FileExistsError as e:
                self.fd = None
                if time.time() + interval < deadline:
                    time.sleep(interval)
                    continue
                self.depth -= 1
                self.tlock.release()
                return False
            except IOError as e:
                self.fd = None
                self.depth -= 1
                self.tlock.release()
                self.lockfile.unlink(missing_ok=True)
                raise

    def release(self):
        with self.tlock:
            if self.fd is None:
                return False

            try:
                if self.depth == 1:
                    try:
                        os.close(self.fd)
                    finally:
                        self.fd = None
                        self.lockfile.unlink()
            finally:
                self.depth -= 1
                self.tlock.release()

            return True

    def __del__(self):
        if self.fd is None:
            return

        try:
            os.close(self.fd)
        finally:
            self.fd = None
            self.lockfile.unlink()


def run_thread(name=None, daemon=None):
    """
    Run the decorated function in a thread with the given parameters, return the already started thread.
    """

    def wrapper(func):
        thread = threading.Thread(target=func, name=name, daemon=daemon)
        thread.start()
        return thread

    return wrapper


def strip_auth(url, redact=None):
    if redact:
        return re.sub("(https?://)([^@/ ]+)@", f"\\1{redact}@", url, re.IGNORECASE)
    else:
        return re.sub("(https?://)([^@/ ]+)@", "\\1", url, re.IGNORECASE)
