import functools
import logging
import json
import bottle
from bottle import Bottle, request, response, HTTPResponse, HTTPError, static_file
import jinja2
import os
import datetime
import time

import chucky
import chucky.config
import chucky.deploy
import chucky.auth
import chucky.sidecar
from chucky.utils import run_thread, strip_auth

log = logging.getLogger(__name__)


def bootstrap(_chucky: "chucky.Chucky"):
    app = Bottle()

    @app.install
    def nice_errors(callback):
        @functools.wraps(callback)
        def wrapper(*a, **ka):
            try:
                return callback(*a, **ka)
            except HTTPResponse as e:
                return e
            except BaseException as e:
                log.exception("Internal server error")
                return error_response(500, "InternalError", repr(e))

        return wrapper

    _build_ui(app, _chucky)
    _build_api(app, _chucky)
    _build_githook(app, _chucky)

    _install_oidc(app, _chucky)  # optional, must happen before rbac
    _install_rbac(app, _chucky)
    _mount_puppedb(app, _chucky)  # optional

    return app


def _build_ui(app, _chucky: "chucky.Chucky"):
    static_dir = os.path.join(os.path.abspath(os.path.dirname(__file__)), "static")
    env = jinja2.Environment(loader=jinja2.PackageLoader(__package__, "templates"))
    env.globals["config"] = _chucky.config
    env.globals["request"] = request
    env.globals["sidecar"] = _chucky.sidecar

    def to_dt(ts):
        if not isinstance(ts, datetime.datetime):
            ts = datetime.datetime.fromtimestamp(ts).astimezone()
        return ts

    def to_date(ts):
        return to_dt(ts).strftime("%c %Z")

    env.filters["to_dt"] = to_dt
    env.filters["to_date"] = to_date
    env.filters["noauth"] = lambda x: strip_auth(x, "***")

    def tpl(name, **args):
        return env.get_template(name).render(**args)

    @app.get("/static/<path:path>")
    def static(path):
        return static_file(path, root=static_dir)

    @app.get("/", oidc=True, roles=("info",))
    @app.get("/envs", oidc=True, roles=("info",))
    def show_env():
        sources = {}
        environments = {}
        for source in _chucky.sources:
            sources[source.name] = {
                "name": source.name,
                "git": source.mirror.safe_url,
                "pattern": source.pattern.pattern,
                "dynamic": source.is_dynamic,
            }
        for env in _chucky.deployer.iter_env_handles():
            if not env.is_materialized:
                continue
            meta = env.read_meta()
            lastrun = env.read_lastrun()
            environments[env.name] = {
                "name": env.name,
                "meta": meta,
                "lastrun": lastrun,
            }

        return tpl(
            "envs.html",
            currentPage="envs",
            sources=sources,
            environments=environments,
            pubkeys=_chucky.sources.git.ssh_pubkeys(),
        )

    @app.get("/env/<envname>", oidc=True, roles=("info",))
    def show_envdetail(envname):
        env = _chucky.deployer.get_env_handle(envname)
        if not env or not env.is_materialized:
            return error_response(404, "UnknownEnvironment", "Environment not found")
        return tpl(
            "env_detail.html",
            currentPage="envs",
            env=env,
            meta=env.read_meta(),
            lastrun=env.read_lastrun(),
            lastlog=env.read_runlog(),
        )

    @app.get("/certs", oidc=True, roles=("info",))
    def show_certs():
        if _chucky.sidecar and _chucky.sidecar.supports("ca-list"):
            cache = chucky.sidecar.DEFAULT_CERT_CACHE
            if "refresh" in bottle.request.query:
                cache = 0
            certs = _chucky.sidecar.ca_list(cache)
        else:
            certs = []

        return tpl(
            "certs.html",
            currentPage="certs",
            certs=certs,
        )

    @app.get("/eyaml", oidc=True, roles=("info",))
    def show_eyaml():
        return tpl("eyaml.html", currentPage="eyaml")

    @app.get("/query", oidc=True, roles=("info",))
    def show_query():
        return tpl("query.html", currentPage="query")


def _build_githook(app, _chucky: "chucky.Chucky"):
    config = _chucky.config
    deployer = _chucky.deployer
    sources = _chucky.sources

    @app.post("/githook")
    @app.post("/githook/<sources>")
    def githook(sources=None):
        if not config.webhook_secret:
            return error_response(
                501, "UnsupportedFeature", "Githooks are currently disabled"
            )

        if sources:
            sources = sources.split(",")

        if request.get_header("X-Gitlab-Event"):
            return gitlab_webhook(sources)

        return error_response(401, "BadEventType", "Unsupported event type")

    def gitlab_webhook(source_names):
        if config.webhook_secret != request.get_header("X-Gitlab-Token"):
            return error_response(401, "BadToken", "Bad or missing api token")

        etype = request.get_header("X-Gitlab-Event")
        if etype != "Push Hook":
            return error_response(
                401, "BadEventType", f"Unsupported event type: {etype}"
            )

        to_sync = dict()
        if source_names:
            for name in source_names:
                if name in to_sync:
                    continue
                source = sources.get_source(name)
                if not source:
                    continue
                to_sync[name] = source
        else:
            event = json.load(request.body)  # pyright: ignore
            log.debug(f"Push event: {event}")
            urls = [
                event["repository"].get(field)
                for field in ["git_http_url", "git_ssh_url"]
            ]
            for source in sources.iter_sources():
                if any(source.match_url(url) for url in urls):
                    to_sync[source.name] = source

        if not to_sync:
            # Not a hard error
            return {"result": "No matches"}

        for source in to_sync.values():
            deployer.sync(source, fetch=True)

        # We do not wait for futures here, as that would take way too long in some cases.
        return {"result": "ok"}


def _build_api(app, _chucky: "chucky.Chucky"):
    deployer = _chucky.deployer
    sources = _chucky.sources
    sidecar = _chucky.sidecar

    @app.get("/health")
    @app.get("/api/v1/health")
    def health():
        if request.query.mode == "simple":  # pyright: ignore
            return "OK Backend service checks were skipped (simple mode)"

        # TODO: Cache for a short while. This API is public
        mode, msg = sidecar.health()
        if mode in ("OK", "UNKNOWN"):
            return f"{mode} {msg}"
        return HTTPResponse(status=500, body=f"{mode} {msg}")

    @app.post("/api/v1/deploy", oidc=True, roles="deploy")
    def trigger_deploy():
        source_name = request.forms.source  # pyright: ignore
        env_name = request.forms.environment or None  # pyright: ignore
        force = request.forms.force.lower() == "true"  # pyright: ignore

        source = sources.get_source(source_name)
        if not source:
            return error_response(
                400, "SourceNotFound", f"Source {source_name!r} not found"
            )

        envlist = env_name.split(",") if env_name else None
        tasks = deployer.sync(source, limit_envs=envlist, fetch=True, force=force)

        answer = {"tasks": []}
        for env, task in tasks.items():
            answer["tasks"].append(
                {
                    "id": task.id,
                    "action": (
                        "deploy"
                        if isinstance(task, chucky.deploy.DeployTask)
                        else "undeploy"
                    ),
                    "environment": env,
                }
            )

        return answer

    @app.get("/api/v1/task/<task_id>", oidc=True, roles="deploy")
    def poll_task(task_id):
        task = _chucky.runner.task_info(task_id)
        if not task:
            return error_response(404, "TaskNotFound", f"Task {task_id!r} not found")
        return task

    @app.post("/api/v1/destroy", oidc=True, roles="deploy")
    def trigger_destroy():
        env = request.forms.environment  # pyright: ignore
        if env:
            env = _chucky.deployer.get_env_handle(env)
        if not env:
            return error_response(
                400, "MissingParameter", "Missing 'environment' parameter"
            )
        task = deployer.destroy(env)
        if not task:
            return error_response(
                404, "EnvironmentNotFound", "No environment with that name"
            )
        task.result(timeout=10)
        return {"result": "ok"}

    @app.get("/api/v1/ca/list", oidc=True, roles="info")
    def ca_listall():
        return sidecar.ca_list()

    @app.post("/api/v1/ca/<action>/<name>", oidc=True, roles="cert")
    def ca_perform(name, action):
        cert = sidecar.find_cert(name)
        if not cert:
            return error_response(404, "CertNotFound", "No certificate with that name")

        if action == "sign":
            if cert["state"] != "requested":
                return error_response(409, "CertState", "Certificate already signed")
            sidecar.ca_sign(name)
        elif action == "revoke":
            if cert["state"] != "signed":
                return error_response(409, "CertState", "Certificate not signed")
            sidecar.ca_revoke(name)
        elif action == "clean":
            sidecar.ca_clean(name)
        else:
            return error_response(400, "UnknownAction", "Requested operation not known")
        return {"result": "ok", "cert": sidecar.find_cert(name)}

    @app.post("/api/v1/secrets/encrypt", oidc=True, roles="eyaml")
    def eyaml_encrypt():
        if not sidecar.supports("eyaml-encrypt"):
            return error_response(
                501, "NotImplemented", "Secret encoding is not configured"
            )
        secret = request.forms.secret  # pyright: ignore
        if not secret:
            return error_response(
                400, "MissingParameter", "Form parameter 'secret' missing"
            )
        return sidecar.eyaml_encrypt(secret)

    @app.post("/api/v1/secrets/rekey", oidc=True, roles="eyaml")
    def eyaml_rekey():
        if not sidecar.supports("eyaml-rekey"):
            return error_response(
                501, "NotImplemented", "Secret re-keying is not supported"
            )
        secret = request.forms.secret  # pyright: ignore
        if not secret:
            return error_response(
                400, "MissingParameter", "Form parameter 'secret' missing"
            )
        return sidecar.eyaml_rekey(secret.strip())

    @app.get("/api/v1/secrets/key", oidc=True, roles="eyaml")
    def eyaml_key():
        if not sidecar.supports("eyaml-key"):
            return error_response(
                501, "NotImplemented", "Secret encoding is not configured"
            )
        key = sidecar.eyaml_key()
        response.content_type = "text/plain"
        response.set_header(
            "Content-Disposition", 'attachment; filename="public_key.pkcs7.pem"'
        )
        return key


def _mount_puppedb(app, _chucky: "chucky.Chucky"):
    import requests

    @app.post("/api/v1/query", oidc=True, roles="board")
    def query_pupeptdb():
        """Query puppetdb. Requests are forwarded verbarim and responses are also unchanged most of the time."""
        if not _chucky.config.puppetdb:
            return error_response(501, "NotImplemented", "PuppetDB is not configured")

        assert isinstance(request.forms, bottle.FormsDict)

        query = {"origin": "Chucky"}
        for name in ("query", "order_by", "limit", "include_total", "offset"):
            value = request.forms.getunicode(name)
            if value:
                query[name] = value

        if not query.get("query"):
            return error_response(
                400, "MissingParameter", "Parameter 'query' is mandatory"
            )

        try:
            url = f"{_chucky.config.puppetdb.rstrip('/')}/pdb/query/v4"
            rs = requests.post(url, json=query)
            if not rs.ok:
                return error_response(rs.status_code, "QueryError", rs.text)
            response.content_type = rs.headers.get("content-type")
            return rs.text
        except requests.exceptions.HTTPError as e:
            return error_response(503, "BadGateway", str(e))

    if not _chucky.config.puppetdb:
        return

    import urllib.parse
    import puppetboard.core

    # Fix app configuration before importing puppetboard.app
    puppetdb = urllib.parse.urlparse(_chucky.config.puppetdb)
    board = puppetboard.core.get_app()
    board.config["SECRET_KEY"] = _chucky.config.secret
    board.config["PUPPETDB_HOST"] = puppetdb.hostname
    board.config["PUPPETDB_PORT"] = puppetdb.port
    board.config["PUPPETDB_PROTO"] = puppetdb.scheme
    board.config["ENABLE_CATALOG"] = True
    board.config["OFFLINE_MODE"] = True  # Prevent use of cdnjs.cloudflare.com

    puppetboard_app = None

    # Just importing puppetboard.app tries to connect to puppetdb as a side
    # effect and throws SystemExit(2) on errors, so we will just try forever
    # in a spearate thread (╯°□°)╯︵ ┻━┻

    @run_thread(name="puppetdb-loader", daemon=False)
    def connect_puppetdb():
        nonlocal puppetboard_app
        delay, delay_backoff, delay_max = 10, 1.2, 120
        while True:
            try:
                import puppetboard.app

                puppetboard_app = puppetboard.app.app
                return
            except SystemExit as e:
                log.warn("Failed to connect puppetboard, trying again ...")
                time.sleep(delay)
                delay = min(delay_max, delay * delay_backoff)

    def delay_wrapper(environ, start_response):
        if not puppetboard_app:
            raise HTTPError(503, "Puppetdb not connected yet")
        return puppetboard_app(environ, start_response)

    app.mount("/board", delay_wrapper, skip=False, oidc=True, roles="board")


def _install_oidc(app, _chucky: "chucky.Chucky"):
    if not _chucky.config.oidc_enabled:
        return

    import beaker.middleware

    beaker_config = {
        "session.type": "file",
        "session.cookie_expires": True,
        "session.key": "chucky-session",
        "session.secret": _chucky.config.secret,
        "session.data_dir": os.path.join(_chucky.config.workdir, "sessions"),
    }
    app.wsgi = beaker.middleware.SessionMiddleware(app.wsgi, beaker_config)

    @app.hook("after_request")
    def _save_session():
        """
        Save non-empty beaker session.

        Autosave unfortunately also saves empty sessions an creates
        tons of useless session files, is we try to avoid that here.
        """
        session = request.get("beaker.session", None)
        if not session:
            return
        if not session.accessed():
            return
        if all(key.startswith("_") for key in session):
            return
        session.save()

    def _login_required():
        """Handle routes that require auth but to not have a valid oidc session"""
        if request.path.startswith("/api"):
            raise error_response(
                401, "Unauthorized", "This API requires authentication"
            )
        else:
            request.oidc.force_login(next_uri=request.url)

    oidc = chucky.auth.OIDCPlugin(
        _chucky.config.oidc_client,
        _chucky.config.oidc_secret,
        config_uri=_chucky.config.oidc_config_uri,
        scopes=_chucky.config.oidc_scopes,
        on_login_required=_login_required,
        get_session=lambda: request["beaker.session"],
        redirect_uri=_chucky.config.oidc_redirect_uri,
    )
    app.install(oidc)


def _install_rbac(app, _chucky: "chucky.Chucky"):
    def get_roles():
        """Fetch roles out of the current user context"""
        if not _chucky.config.oidc_enabled:
            # Without OIDC, everyone is admin
            return {"admin"}

        claims = request.oidc.claims()
        sso_id = claims.get(_chucky.config.oidc_id_claim, None)
        sso_roles = set()
        for claim in _chucky.config.oidc_role_claims:
            _roles = claims.get(claim, [])
            if not isinstance(_roles, (list, tuple, set)):
                _roles = [_roles]
            sso_roles.update(_roles)
        if claims:
            sso_roles.add("")  # The empty role means: User is logged in.
        roles = set(b for a, b in _chucky.config.oidc_role_mapping if a in sso_roles)
        roles.update(b for a, b in _chucky.config.oidc_id_mapping if a == sso_id)
        log.debug(
            "RBAC check for SSO user %r with SSO roles %r. Mapped permissions: %r",
            sso_id,
            sso_roles,
            roles,
        )
        return roles

    def _on_badroles(roles, allowed):
        """Handle user not in any of the required roles for a route"""
        user_id = (
            request.oidc.claims().get(_chucky.config.oidc_id_claim, None)
            if hasattr(request, "oidc")
            else None
        )
        msg = "RBAC access denied for %r. User roles %r do not match any of the allowed roles %r "
        log.warn(msg, user_id, roles, allowed)
        return error_response(
            403, "Forbidden", "Missing required roles or permissions."
        )

    roles = chucky.auth.RBACPlugin(get_roles=get_roles, on_badroles=_on_badroles)
    roles.register("admin", implies="dev")
    roles.register("dev", implies="cert eyaml deploy board")
    roles.register("cert", implies="info")
    roles.register("eyaml", implies="info")
    roles.register("deploy", implies="info")
    roles.register("board", implies="info")
    roles.register("info")
    app.install(roles)


def error_response(status, name, message, **args):
    if request.path.startswith("/api"):
        return HTTPResponse(
            status=status,
            body={"error": name, "message": message, **args},  # pyright: ignore
        )
    return HTTPError(status=status, body=f"{name}: {message}")
