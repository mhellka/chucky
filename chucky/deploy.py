from pathlib import Path
import re
from chucky.sidecar import Sidecar, SidecarError, SidecarResult
from chucky.sources import RefInfo, Source
from chucky.tasks import TaskRunner, BaseTask
from .utils import FileLock
import logging
import time
import json
import shutil
from typing import Iterable, Optional

log = logging.getLogger(__name__)


class DeployError(RuntimeError):
    pass


class DeployService:
    def __init__(self, workdir: Path, sidecar: Sidecar, runner: TaskRunner):
        self.workdir = workdir.absolute()
        self.runner = runner
        self.sidecar = sidecar
        self.tasks = {}

    def iter_env_names(self):
        for metafile in self.workdir.glob("*.meta"):
            yield metafile.stem

    def iter_env_handles(self):
        for name in self.iter_env_names():
            yield self.get_env_handle(name)

    def get_env_handle(self, name):
        """Get a named environment handle. It may not exist yet."""
        return EnvHandle(self, name)

    def sync(
        self,
        source: Source,
        limit_envs: Optional[Iterable[str]] = None,
        fetch=True,
        force=False,
        delay=False,
    ) -> dict[str, "DeployTask|UndeployTask"]:
        """Create a SyncTask

        @param delay: Do not start task, just create and return it.
        """

        # Collect tasks while source is locked
        tasks: dict[str, "DeployTask|UndeployTask"] = {}
        with source.lock(timeout=10):

            if fetch:
                source.fetch()

            for env_name, ref in source.get_envs():
                if limit_envs and env_name not in limit_envs:
                    continue
                env = self.get_env_handle(env_name)
                tasks[env.name] = DeployTask(self, env, source, ref, force=force)

            # Find and undeploy orphans
            if source.is_dynamic:
                for env in self.iter_env_handles():
                    if limit_envs and env.name not in limit_envs:
                        continue
                    if env.name in tasks:
                        continue  # Not an orphan, we willd re-deploy this one
                    if not env.claimed_to(source.name):
                        continue  # Not claimed by this source
                    tasks[env.name] = UndeployTask(self, env, source)

        # Submit all those tasks, but do NOT wait for them
        if not delay:
            for task in tasks.values():
                task.submit()

        return tasks

    def destroy(self, env: "EnvHandle"):
        task = UndeployTask(self, env, None)
        task.submit()
        return task


class EnvHandle:
    """Representation of a single environment."""

    def __init__(self, repo: DeployService, name):
        self.repo = repo
        self.name = name
        self.envdir = repo.workdir / f"{name}.env"
        self.metafile = repo.workdir / f"{name}.meta"
        self.runfile = repo.workdir / f"{name}.lastrun"
        self.logfile = repo.workdir / f"{name}.lastlog"
        self.lock = FileLock(repo.workdir / f"{name}.lock")

    def __eq__(self, other: object) -> bool:
        return isinstance(other, EnvHandle) and other.name == self.name

    def read_meta(self):
        try:
            return json.loads(self.metafile.read_text())
        except FileNotFoundError:
            return {}

    def write_meta(self, meta):
        self.metafile.write_text(json.dumps(meta))

    def read_lastrun(self):
        try:
            return json.loads(self.runfile.read_text())
        except FileNotFoundError:
            return {}

    def write_lastrun(self, data):
        self.runfile.write_text(json.dumps(data))

    def read_runlog(self):
        try:
            return json.loads(self.logfile.read_text())
        except FileNotFoundError:
            return {}

    def write_runlog(self, data):
        self.logfile.write_text(json.dumps(data))

    def try_claim(self, source_id: str, check_only=False):
        """
        Ensure that this environment belongs to a given current source, or
        try to claim it. Return True on success, False on conflicts with a
        different source.

        If only_check is True and the environment is not yet claimed, do not
        claim it and return None instead.
        """
        with self.lock:
            if self.metafile.exists():
                return self.read_meta().get("source") == source_id
            elif not check_only:
                self.write_meta({"source": source_id})
                return True

    def claimed_to(self, source_id: str):
        return self.read_meta().get("source") == source_id

    def exists(self):
        return self.metafile.exists()

    def is_materialized(self):
        return self.envdir.exists()

    def is_locked(self):
        return self.lock.is_locked()

    def _destroy(self):
        with self.lock:
            self.metafile.unlink(missing_ok=True)
            self.runfile.unlink(missing_ok=True)
            shutil.rmtree(self.envdir, ignore_errors=True)


class DeployTask(BaseTask):
    """Task to deploy a single environment."""

    def __init__(
        self,
        deploy: DeployService,
        env: EnvHandle,
        source: Source,
        ref: RefInfo,
        force=False,
    ):
        super().__init__(deploy.runner)
        self.sidecar = deploy.sidecar
        self.env = env
        self.source = source
        self.ref = ref
        self.force = force

    def __repr__(self):
        return f"DeployTask(source={self.source.name}, env={self.env.name}, commit={self.ref.ref})"

    def _run(self):
        with self.env.lock(timeout=10):
            return self._deploy()

    def _deploy(self):
        assert self.ref

        ref = self.ref
        env = self.env
        source = self.source
        mirror = source.mirror

        # Claim environment if not yet claimed
        if not env.try_claim(self.source.name):
            raise DeployError(
                f"Environment {env.name} does not belong to source {source.name}"
            )

        lastrun = env.read_lastrun()

        if (
            not self.force
            and lastrun
            and "error" not in lastrun
            and lastrun["commit"] == ref.ref
        ):
            self.log(f"Nothing to do for: {env}")
            self.skipped = True
            return lastrun

        lastrun = {
            "source": source.name,
            "origin": mirror.safe_url,
            "commit": ref.ref,
            "branch": ref.branch,
            "started": self.ts_start,
            # completed: float
            # failed: float
            # error: str
        }

        try:
            self.log(f"Checkout [{ref.ref[:8]}] ({ref.branch or ref.name})")
            mirror.checkout(self.env.envdir, self.ref)

            if self.sidecar.supports("deploy"):
                self.log(f"Run sidecar deploy...")
                try:
                    result = self.sidecar.deploy(env.name, env.envdir)
                    self._log_sidecar(result)
                except SidecarError as e:
                    if e.result:
                        self._log_sidecar(e.result)
                    raise

            self.log(f"Deploy complete!")
            lastrun["completed"] = time.time()
            env.write_lastrun(lastrun)
            env.write_runlog(sorted(self.tasklog))
            return lastrun
        except Exception as e:
            self.log(f"Deploy failed!", logging.ERROR)
            lastrun["failed"] = time.time()
            lastrun["error"] = str(e)
            env.write_lastrun(lastrun)
            env.write_runlog(sorted(self.tasklog))
            raise

    def _log_sidecar(self, result: SidecarResult):
        logs = [(line, ts, logging.ERROR) for ts, line in result.stderr_log]
        logs += [(line, ts, logging.INFO) for ts, line in result.stdout_log]
        logs.sort(key=lambda x: x[1])
        for line, ts, level in logs:
            line = line.rstrip()  # remove newline
            line = re.sub("\x1b\\[[0-9;]*m", "", line)  # remove shell formating
            line = line.replace("\x1b", "^")  # escape other shell sequences
            self.log(line, level=level, ts=ts)
        self.log(f"Sidecar command exited with status: {result.status}")


class UndeployTask(BaseTask):
    """Task to undeploy (cleanup and remove) a single environment."""

    def __init__(
        self, deploy: DeployService, env: EnvHandle, source: Optional[Source] = None
    ):
        super().__init__(deploy.runner)
        self.env = env
        self.source = source
        self.sidecar = deploy.sidecar

    def __repr__(self):
        return f"UndeployTask(env={self.env.name})"

    def _run(self):
        try:
            self.log(f"Undeploy triggered for: {self.env.name}")
            with self.env.lock(timeout=10):
                return self._destroy()
        except Exception as e:
            self.log(f"Undeploy failed for {self.env.name}", logging.ERROR)
            raise

    def _destroy(self):
        if not self.env.exists():
            raise DeployError(f"Environment {self.env.name} does not exist")

        if self.source and not self.env.claimed_to(self.source.name):
            raise DeployError(
                f"Environment {self.env.name} does not belong to {self.source}"
            )

        with self.env.lock(timeout=10):
            self.env._destroy()
            if self.sidecar.supports("undeploy"):
                self.log(f"Run sidecar undeploy...")
                self.sidecar.undeploy(self.env.name)
