import hashlib
import os, yaml
import logging
from pathlib import Path
from typing import Any, Literal
from dataclasses import dataclass

log = logging.getLogger(__name__)

DEFAULT_CONFIG = "/etc/chucky/config.yaml"


@dataclass(init=False)
class ChuckyConfig:
    sidecar: str
    sources: Any

    oidc_enabled: bool = False
    oidc_role_claims: list[str]
    puppetdb: str | Literal[False] = False

    def load(self, env_prefix="CHUCKY_", config_location=DEFAULT_CONFIG):
        yaml_config = {}
        config_file = os.environ.get(f"{env_prefix}CONF", config_location)
        if Path(config_file).exists():
            with open(config_file) as fp:
                yaml_config = yaml.load(fp, Loader=yaml.SafeLoader)

        self.sources = yaml_config.get("sources", {})

        def _getconf(name, default: Any = "__FAIL__") -> Any:
            env_name = f"{env_prefix}{name.upper()}"
            value = os.environ.get(env_name) or yaml_config.get(name)
            if value is not None:
                return value
            if default != "__FAIL__":
                return default
            raise RuntimeError(
                f"Mandatory config parameter {name!r} not found in environment (as {env_name}) or in {config_file!r}"
            )

        def _list(value):
            if not value:
                return []
            if isinstance(value, str):
                return value.split(",")
            return list(value)

        self.secret = _getconf("secret")
        self.webhook_secret = _getconf("webhook_secret", "")
        if not self.webhook_secret:
            self.webhook_secret = derive_secret(self.secret, "webhook")
        self.workdir = Path(_getconf("workdir"))
        self.sidecar = _getconf("sidecar")

        self.protected_certs = [
            name.strip()
            for name in _list(_getconf("protected_certs", "puppet,puppetdb"))
            if name.strip()
        ]

        self.oidc_enabled = _getconf("oidc_client", False)
        if self.oidc_enabled:
            self.oidc_client = _getconf("oidc_client")
            self.oidc_config_uri = _getconf("oidc_config_uri")
            self.oidc_redirect_uri = _getconf("oidc_redirect_uri", "/oidc")
            self.oidc_secret = _getconf("oidc_secret")
            self.oidc_scopes = _getconf("oidc_scopes", "openid")
            self.oidc_id_claim = _getconf("oidc_id_claim", "sub")
            self.oidc_name_claim = _getconf("oidc_name_claim", "name")
            self.oidc_role_claims = _list(_getconf("oidc_role_claim", "roles"))
            _roles = "admin dev cert eyaml deploy info board".split()
            _rolemap_default = ",".join(f"chucky_{role}={role}" for role in _roles)
            _rolemap = _getconf("oidc_role_mapping", _rolemap_default)
            self.oidc_role_mapping = [rule.split("=", 1) for rule in _list(_rolemap)]
            _idmap = _getconf("oidc_id_mapping", "")
            self.oidc_id_mapping = [rule.split("=", 1) for rule in _list(_idmap)]

        self.public_url = _getconf("public_url", "/")
        self.puppetserver = _getconf("puppetserver", False)
        self.puppetdb = _getconf("puppetdb", False)

        self.links = _getconf("links", [])
        if isinstance(self.links, str):
            tmp = _list(self.links)
            self.links = []
            for link in tmp:
                if " " in link:
                    title, _, url = link.rpartition(" ")
                    self.links.append({"title": title, "url": url})


def derive_secret(seed, name):
    return hashlib.pbkdf2_hmac(
        "sha256",
        password=seed.encode("utf8"),
        salt=hashlib.sha256(name.encode("utf8")).digest(),
        iterations=1000,
    ).hex()
