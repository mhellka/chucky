import io
import shlex
import shutil
import subprocess
import tarfile
import tempfile
from pathlib import Path
import threading
import time
import logging
from typing import Iterable

log = logging.getLogger(__name__)


class SidecarError(RuntimeError):
    def __init__(self, msg, result=None):
        self.result = result
        super().__init__(msg)


DEFAULT_PROTECTED = frozenset(["puppet", "puppetdb"])
DEFAULT_CERT_CACHE = 10
DEFAULT_SIDECAR_TIMEOUT = 30
DEFAULT_DEPLOY_TIMEOUT = 120


class Sidecar:
    def __init__(self, command, protected_certs: Iterable[str] = DEFAULT_PROTECTED):
        if not isinstance(command, list):
            command = shlex.split(command)
        self.cmd = command[:]
        self.protected_certs = set(protected_certs or [])
        self._caps = []
        self._calist_cache = None
        self._calist_refresh = 0

    def reset(self):
        self._caps = []

    def caps(self, refresh=False):
        """Return vesion and list of capabilities.
        If not yet known, or if `refresh` is true, this may fail with `SidecarError`.
        """

        if self._caps and not refresh:
            return self._caps[0], self._caps[1:]

        try:
            caps = self._run("caps").stdout_first_line.split() or ["EMPTY"]
            if caps[0] != "v0":
                raise SidecarError(f"Unsupported sidecar version: {caps[0]}")
            self._caps = caps
            return caps[0], caps[1:]
        except SidecarError as e:
            self._caps = []
            raise SidecarError(
                f"Sidecar failed to report version and capabilities", e.result
            )

    @property
    def version(self):
        """Sidecar protocol version string, or None if unknown."""
        return self._caps[0] if self._caps else None

    @property
    def capabilities(self):
        """Sidecar capabilities, or an empty list if unknown."""
        return self._caps[0] if self._caps else None

    def supports(self, command, refresh=False):
        """Check if sidecar supports the given command.

        If refresh is true, contat sidecar again.
        """
        try:
            return command in self.caps(refresh=refresh)[1]
        except SidecarError:
            return False

    def _run(self, command, *args, stdin=None, check=True, timeout=None):
        if command != "caps" and not self.supports(command):
            raise SidecarError(f"Command {command!r} not supported by sidecar")

        cmd = self.cmd + [command] + list(args)
        kwargs = {}
        kwargs["stdin"] = subprocess.PIPE if stdin else None
        kwargs["stdout"] = subprocess.PIPE
        kwargs["stderr"] = subprocess.PIPE
        kwargs["bufsize"] = 1
        kwargs["encoding"] = "utf-8"

        log.debug(f"Running sidecar: {cmd!r}")
        proc = subprocess.Popen(cmd, **kwargs)

        stdout, stderr = [], []

        def logoutput(stream, line):
            now = time.time()
            stream.append((now, line))
            chan = "stdout" if stream is stdout else "stderr"
            log.debug(f"Sidecar {chan}: {line.rstrip()}")

        _watch_stream(proc.stdout, lambda line: logoutput(stdout, line))
        _watch_stream(proc.stderr, lambda line: logoutput(stderr, line))

        if isinstance(proc.stdin, io.TextIOWrapper):
            try:
                try:
                    if isinstance(stdin, str):
                        proc.stdin.write(stdin)
                    elif isinstance(stdin, io.IOBase):
                        shutil.copyfileobj(stdin, proc.stdin.buffer)
                finally:
                    proc.stdin.close()
            except (BrokenPipeError, OSError):
                pass

        def procwait(timeout=None):
            try:
                proc.wait(timeout=timeout)
                return True
            except subprocess.TimeoutExpired:
                return False

        if not procwait(timeout):
            log.warn(f"Sidecar command {cmd!r} timed out")
            proc.terminate()

        if not procwait(timeout):
            log.error(f"Sidecar command {cmd!r} failed to terminate")
            proc.kill()

        code = proc.wait()
        if check and code != 0:
            err = f"Sidecar command {command!r} failed with code {code}"
            log.error(err)
            raise SidecarError(err, SidecarResult(code, stdout, stderr))

        return SidecarResult(code, stdout, stderr)

    def health(self):
        try:
            if not self.supports("health"):
                return "UNKNOWN", "Health command not supported"
            result = self._run("health", check=False)
            output = result.stdout_first_line or "No Output"
            return ("OK" if result.status == 0 else "ERROR"), (output)
        except SidecarError as e:
            return "ERROR", str(e)

    def deploy(
        self, envname: str, sourcepath: Path, timeout=DEFAULT_DEPLOY_TIMEOUT
    ) -> "SidecarResult":
        def tar_filter(tarinfo):
            if tarinfo.name.endswith(".git"):
                return None
            tarinfo.uid = tarinfo.gid = 0
            tarinfo.uname = tarinfo.gname = "root"
            return tarinfo

        with tempfile.TemporaryFile() as tmp:
            tar = tarfile.open(fileobj=tmp, mode="w")
            tar.add(sourcepath, arcname=envname, filter=tar_filter)
            tar.close()
            tmp.seek(0)
            return self._run("deploy", envname, stdin=tmp, timeout=timeout)

    def undeploy(self, envname):
        self._run("undeploy", envname)

    def eyaml_encrypt(self, secret):
        return self._run("eyaml-encrypt", stdin=secret).stdout

    def eyaml_key(self):
        return self._run("eyaml-key").stdout

    def eyaml_rekey(self, encrypted):
        return self._run("eyaml-rekey", stdin=encrypted).stdout

    def _ca_clear_cache(self):
        self._calist_refresh = 0

    def ca_list(self, cache_timeout=DEFAULT_CERT_CACHE):
        """Fetch and return CA list.
        If cache_timeout is a positive number, cached results may be returned if newer than the cache timeout.
        """

        if (
            self._calist_cache
            and cache_timeout > 0
            and time.time() - self._calist_refresh < cache_timeout
        ):
            return self._calist_cache[:]

        certs = []
        for line in self._run("ca-list").stdout.splitlines():
            fields = line.split("\t", 3)
            if not len(fields) == 4:
                log.warn(f"Bad response to ca-list command: {line!r}")
                continue
            name, state, fingerprint, info = fields
            if state not in ("requested", "signed", "revoked"):
                log.warn(f"CAList returned unsupported state. Line was: {line!r}")
                continue
            certs.append(
                {
                    "name": name.strip(),
                    "state": state.strip(),
                    "fingerprint": fingerprint.strip(),
                    "info": info.strip(),
                    "protected": name.strip() in self.protected_certs,
                }
            )
        self._calist_cache = certs
        self._calist_refresh = time.time()
        return self._calist_cache[:]

    def find_cert(self, name, cache_timeout=DEFAULT_CERT_CACHE):
        for cert in self.ca_list(cache_timeout=cache_timeout):
            if cert["name"] == name:
                return cert

    def ca_sign(self, certname):
        if not self.find_cert(certname):
            return False
        self._run("ca-sign", certname)
        self._ca_clear_cache()

    def ca_revoke(self, certname):
        if not self.find_cert(certname):
            return False
        self._run("ca-revoke", certname)
        self._ca_clear_cache()

    def ca_clean(self, certname):
        if not self.find_cert(certname):
            return False
        self._run("ca-clean", certname)
        self._ca_clear_cache()


class SidecarResult:
    def __init__(
        self,
        status: int,
        stdout: list[tuple[float, str]],
        stderr: list[tuple[float, str]],
    ):
        self._status = status
        self._stdout = stdout
        self._stderr = stderr

    @property
    def status(self):
        return self._status

    @property
    def stdout_first_line(self):
        return self._stdout[0][1] if self._stdout else ""

    @property
    def stdout(self):
        return "".join(l[1] for l in self._stdout)

    @property
    def stdout_log(self):
        return self._stdout or []

    @property
    def stderr(self):
        return "".join(l[1] for l in self._stderr)

    @property
    def stderr_log(self):
        return self._stderr or []


def _watch_stream(stream, callback):
    """
    Start a background thread to read lines from a blocking stream
    and trigger a callback on each line.
    """

    def _reader(stream, callback):
        for line in stream:
            callback(line)

    if stream:
        threading.Thread(target=_reader, args=(stream, callback), daemon=True).start()
