import inspect
from typing import Callable, Optional
import bottle
from urllib.parse import urljoin
import requests
import authlib.oidc.core
import authlib.jose
import authlib.integrations.requests_client
import authlib.common.errors
import logging

log = logging.getLogger(__name__)


class OIDCPlugin:
    """
    Bottle plugin to authenticate users against an OpenID Connect provider.

    Given a `config_uri`, this plugin can mostly configure itself. The provider
    should allow `https://your-domain.com/oidc` as a valid `redirect_uri`.

    Routes that either have an `oidc` route config parameter or expect an `oidc_claims`
    keyword argument will trigger an authentication flow if there is not already a
    valid token associated with the current user session. The route itself can access
    claims or use the plugin via `request.oidc` and its methods.

    The `oidc` route config parameter can have multible values:
    - `True`: Force login
    - `'optional'`: Allow guests
    - callable: Call this function with all claims, which might be an empty dict for
      guests. If it returns True, the request is accepted. Otherwise, login is enforced.
      The callable might also short-circut request handling by throwing an HTTPResponse.

    This plugin needs a user-bound session store that can hold more than 4k per key.
    Cookies may not be enough, but `beaker.middleware.SessionMiddleware` works great.
    """

    name = "oidc"
    api = 2

    def __init__(
        self,
        client_id: str,
        client_secret: str,
        get_session: Callable,
        on_login_required: Optional[Callable] = None,
        redirect_uri="/oidc",
        default_login_url="/",
        default_logout_url="/",
        scopes="openid",
        config_uri: Optional[str] = None,
        authorization_endpoint: Optional[str] = None,
        token_endpoint: Optional[str] = None,
        end_session_endpoint: Optional[str] = None,
        revocation_endpoint: Optional[str] = None,
        jwks: Optional[str | dict] = None,
    ):
        """
        Create and configure a new Plugin Instance

        :param str client_id: OAuth client ID
        :param str client_secret: OAuth client secret
        :param callable get_session: Callable to get a user-bound session (a dict-like object)
          from the environment. If beaker is used, this should be `lambda: bottle.request['beaker.session']`
        :param callable on_login_required: Called instead of `self.force_login()` if a required
          session is not present. Should either trigger force_login or redirect to a login page.
        :param str redirect_uri: Alternative OAuth redirect_uri (default: `/oauth`)
        :param str default_login_url: Default redirect URI after login if not specified.
        :param str default_logout_url: Default redirect URI after logout if not specified.
        :param str scopes: Space-separated list of scopes requested for this session.
        :param str config_uri: An OpenID configuration url (usually http://provider.tld/.well-known/openid-configuration).
          If set, all remmaining keyword arguments are auto-configured.
        :param str authorization_endpoint: OpenID authorization endpoint.
        :param str token_endpoint: OpenID token endpoint.
        :param str end_session_endpoint: OpenID end-session endpoint. (optional)
        :param str revocation_endpoint: OpenID token revokation endpoint. (optional)
        :param str/dict jwks: URI to download JWKS or the already parsed json structure.
        """
        self.app = None
        self.cookie_name = "oidc"
        self.client_id = client_id
        self.client_secret = client_secret
        self.default_login_url = default_login_url or "/"
        self.default_logout_url = default_logout_url or "/"
        self.redirect_uri = redirect_uri or "/oidc"
        self.scopes = scopes or "openid"

        self.get_session = get_session
        self.on_login_required = on_login_required

        if config_uri:
            autoconf = requests.get(config_uri).json()
            if not authorization_endpoint:
                authorization_endpoint = autoconf["authorization_endpoint"]
            if not token_endpoint:
                token_endpoint = autoconf["token_endpoint"]
            if not end_session_endpoint:
                end_session_endpoint = autoconf.get("end_session_endpoint")
            if not revocation_endpoint:
                revocation_endpoint = autoconf.get("revocation_endpoint")
            if not jwks:
                jwks = autoconf["jwks_uri"]

        if not authorization_endpoint:
            raise ValueError("Either config_uri or authorization_endpoint must be set")
        if not jwks:
            raise ValueError("Either config_uri or jwks must be set")

        if not isinstance(jwks, dict):
            jwks = requests.get(jwks).json()

        self.authorization_endpoint = authorization_endpoint
        self.token_endpoint = token_endpoint
        self.end_session_endpoint = end_session_endpoint
        self.revocation_endpoint = revocation_endpoint
        self.jwks = jwks

    def setup(self, app):
        """Part of the bottle Plugin API. Do not call directly."""
        if self.app:
            raise RuntimeError("Plugin already bound")
        self.app = app
        self.app.get("/oidc", callback=self.handle_callback)
        self.app.get("/oidc/login", callback=self.force_login)
        self.app.post("/oidc/logout", callback=self.force_logout)

    def apply(self, callback, route):
        """Part of the bottle Plugin API. Do not call directly."""
        args = [
            p.name
            for p in inspect.signature(callback).parameters.values()
            if p.kind in (p.POSITIONAL_OR_KEYWORD, p.KEYWORD_ONLY)
        ]
        oidc_check = route.config.get("oidc") or "oidc_claims" in args

        if not oidc_check:
            return callback

        def wrapper(*a, **ka):
            bottle.request.oidc = self
            claims = self.claims()
            if "oidc_claims" in args:
                ka["oidc_claims"] = claims
            if callable(oidc_check) and oidc_check(claims):
                return callback(*a, **ka)
            if claims or oidc_check == "optional":
                return callback(*a, **ka)

            if self.on_login_required:
                self.on_login_required()
            else:
                self.force_login(next_uri=bottle.request.url)

        return wrapper

    def _oauth2session(self, **opts):
        opts.setdefault("client_id", self.client_id)
        opts.setdefault("client_secret", self.client_secret)
        opts.setdefault("scope", self.scopes)
        opts.setdefault("token_endpoint", self.token_endpoint)
        opts.setdefault("authorization_endpoint", self.authorization_endpoint)
        opts.setdefault("update_token", self._update_token)
        return authlib.integrations.requests_client.OAuth2Session(**opts)

    def _update_token(self, token, refresh_token=None, access_token=None):
        # 'refresh_token' or 'access_token' contain the old values. 'token' is new.
        self.set_state("token", token)

    def set_state(self, name, value):
        """Stoie a value in the user session.

        The key name is prefixed with 'oidc_'.
        If the value is None, the key is removed instead.
        """
        sess = self.get_session()
        name = "oidc_" + name
        if value:
            sess[name] = value
        elif name in sess:
            del sess[name]

    def get_state(self, name):
        """Get a value stored in the user session."""
        sess = self.get_session()
        name = "oidc_" + name
        return sess.get(name)

    def prepare_login(self, next_uri=None):
        """
        Prepare a new authorisation flow and return the URL the client needs to be redirected to.
        State and next url are stored in the user session.
        """
        redirect = urljoin(bottle.request.url, self.redirect_uri)
        with self._oauth2session() as sess:
            url, state = sess.create_authorization_url(
                self.authorization_endpoint, redirect_uri=redirect
            )
        if next_uri and next_uri.startswith(redirect):
            next_uri = self.default_login_url  # Prevent redirect loops
        self.set_state("next", next_uri)
        self.set_state("state", state)
        return url

    def force_login(self, next_uri=None):
        """
        Trigger a new authorisation flow.

        This will redirect the user to the OpenID Connect provider even if the user is
        already authenticated. If `next_uri` is present, users will be redirected to that
        URI on success.
        """
        bottle.redirect(self.prepare_login(next_uri))

    def force_logout(self, next_uri=None):
        """
        Trigger a logout.

        This will always 'forget' the current session and also try
        to revoke tokens (if `revocation_endpoint` is known) and eventuelly
        redirect the user to `end_session_endpoint` if known.
        """
        from urllib.parse import urlencode

        post_logout = urljoin(bottle.request.url, next_uri or self.default_logout_url)

        token = self.get_state("token")
        if not token:
            bottle.redirect(post_logout)

        self.set_state("token", None)

        with self._oauth2session(token=token) as sess:
            if self.revocation_endpoint:
                sess.revoke_token(self.revocation_endpoint)
            if self.end_session_endpoint and "id_token" in token:
                params = {
                    "id_token_hint": token["id_token"],
                    "post_logout_redirect_uri": post_logout,
                }
                end_session_uri = self.end_session_endpoint + "?" + urlencode(params)
                bottle.redirect(end_session_uri)
            bottle.redirect(post_logout)

    def handle_callback(self):
        """Callback handler for users coming back from OIDC provider"""
        state = self.get_state("state")
        next_url = self.get_state("next")

        try:
            if not state:
                raise bottle.HTTPError(400, "Session currently not expecting a login")

            if state != bottle.request.query.state:  # pyright: ignore
                raise bottle.HTTPError(
                    400, "Bad state. Do not bookmark or reload this URL"
                )

            if bottle.request.query.error:  # pyright: ignore
                error = bottle.request.query.error  # pyright: ignore
                desc = (
                    bottle.request.query.error_description  # pyright: ignore
                    or "No description"
                )
                raise bottle.HTTPError(401, f"Login failed with error {error}: {desc}")

            redirect = urljoin(bottle.request.url, self.redirect_uri)
            with self._oauth2session(state=state, redirect_uri=redirect) as sess:
                token = sess.fetch_token(authorization_response=bottle.request.url)

        except:
            self.set_state("state", None)
            self.set_state("next", None)
            raise

        self.set_state("token", token)
        self.set_state("next", None)
        bottle.redirect(next_url or self.default_login_url)

    def claim(self, name, fallback=None):
        """Return a specific claim, or a fallabck value"""
        return self.claims().get(name, fallback)

    def claims(self):
        """
        Return claims associated to the currently logged-in user,
        or an empty dict if the user is not logged in. Expired tokens
        will be refreshed if needed.
        """

        claims = getattr(bottle.request, "oidc_claims", None)
        if claims is not None:
            return claims

        token = self.get_state("token")
        if not token or "id_token" not in token:
            bottle.request.oidc_claims = {}
            return {}

        try:
            if token.is_expired():
                with self._oauth2session(token=token) as sess:
                    token = sess.refresh_token(self.token_endpoint)
                    self.set_state("token", token)

            claims = authlib.jose.jwt.decode(
                token["id_token"],
                self.jwks,
                claims_cls=authlib.oidc.core.CodeIDToken,
            )
            claims.validate()

            bottle.request.oidc_claims = claims
            return claims

        except authlib.common.errors.AuthlibBaseError:
            log.exception("Failed to refresh or decode token")
            bottle.request.oidc_claims = {}
            self.set_state("token", None)
            return {}


class RBACPlugin:
    """
    Plugin that ensures certain routes can only be accessed by users
    assigned one of the allowed roles.

    Usage:
    rbac = app.install(RBACPlugin(get_roles=lambda: find_user_roles_for_current_user))
    rbac.register("admin", implies=("manager", "employee"))
    rbac.register("manager", implies=("employee",))
    rbac.register("employee")

    @app.get('/protected', roles=('admin', 'manager'))
    def protected():
        request.roles == request.rbac.roles == rbac.roles

    """

    name = "rbac"
    api = 2

    def __init__(self, get_roles, on_badroles=None):
        self.get_roles = get_roles
        self.on_badroles = on_badroles
        self.all_roles = {}  # Maps role to set(implied roles)

        self.cached_implies = {}
        self.cached_implied_by = {}

    def register(self, role, implies=[]):
        """Register a role.

        A role can imply other roles. Example: If ADMIN implies USER, then
        ADMINs can do anything a USER can (and more). In other words: All ADMINs
        are USERs, even if they are not explicitly associated with the USER role.
        This works recursively: If ADMIN implies MANAGER and MANAGER implies USER,
        then ADMIN implies both MANAGER and USER.

        """
        for irole in set(implies.split() if isinstance(implies, str) else implies):
            self.all_roles.setdefault(role, set()).add(irole)
            self.all_roles.setdefault(irole, set())
        self.cached_implies.clear()
        self.cached_implied_by.clear()

    def resolve_implies(self, role):
        """
        Return all roles that imply the given role, including the given role.

        If an action requires the given role, any role that implies the given role
        would also be allowed to perform the action. The returned roles have the
        same or more permissions than the given role.
        """
        result = self.cached_implies.get(role)
        if result:
            return result

        to_check, result = [role], set()
        while to_check:
            check = to_check.pop()
            if check in result:
                continue
            result.add(check)
            to_check.extend(
                role for role, implies in self.all_roles.items() if check in implies
            )
        self.cached_implies[role] = result
        return result

    def resolve_implied_by(self, role):
        """
        Return all roles that are implied by the given role, including the given role.

        If a user has the given role, he also has all the implied roles.
        """
        result = self.cached_implied_by.get(role)
        if result:
            return result

        to_check, result = [role], set()
        while to_check:
            check = to_check.pop()
            if check in result:
                continue
            result.add(check)
            to_check.extend(self.all_roles.get(check, []))
        self.cached_implied_by[role] = result
        return result

    def user_roles(self):
        """All roles returned by self.get_roles() including implied roles."""
        roles = getattr(bottle.request, "roles", None)
        if not roles:
            roles = set()
            for role in self.get_roles():
                roles.update(self.resolve_implied_by(role))
            roles = bottle.request.roles = roles
        return roles

    def match(self, *roles):
        """
        Check if the current user has one of the expected roles, or a role that implies one of the expected roles.
        """
        user_roles = self.user_roles()
        return any(role in user_roles for role in roles)

    def apply(self, callback, route):

        allow_anyone = False
        allowed = route.config.get("roles", [])
        if not allowed:
            return callback

        if isinstance(allowed, str):
            allowed = allowed.split()
        elif allowed is True:
            allow_anyone = True
            allowed = []

        allowed = frozenset(allowed)

        def wrapper(*a, **ka):

            bottle.request.rbac = self
            if allow_anyone or any(role in allowed for role in self.user_roles()):
                return callback(*a, **ka)

            if self.on_badroles:
                return self.on_badroles(self.user_roles(), allowed)

            err = f"Account not in roles: {', '.join(allowed)}"
            raise bottle.HTTPError(403, err)

        return wrapper
