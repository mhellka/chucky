import dataclasses
from functools import cached_property
from pathlib import Path
from hashlib import sha256
import re

from .utils import FileLock, strip_auth
import subprocess
import logging
import json
from typing import Iterable, Optional
from datetime import datetime as Datetime, timezone

log = logging.getLogger(__name__)


class SourceError(RuntimeError):
    pass


class GitHelper:
    def __init__(self, home: Path | None = None):
        self.home = home or Path.home()

    def run(self, *cmd, cwd: Path | None = None, check=True, env=None, timeout=None):
        if len(cmd) == 1 and isinstance(cmd[0], (list, tuple)):
            cmd = list(cmd[0])
        args = {}
        args["encoding"] = "utf8"
        if timeout:
            args["timeout"] = timeout
        args["input"] = ""
        args["stdout"] = subprocess.PIPE
        args["stderr"] = subprocess.STDOUT
        args["cwd"] = str(cwd) if cwd else "/"
        args["env"] = {
            "GIT_SSH_COMMAND": "ssh -o StrictHostKeyChecking=accept-new",
            "HOME": str(self.home.absolute()),
            "LANG": "C",
        }

        if env:
            args["env"].update(env)

        try:
            proc = subprocess.run(cmd, **args)
        except subprocess.TimeoutExpired:
            log.warn(f'Command {" ".join(cmd)} in {cwd} timed out')
            raise
        except BaseException as e:
            log.warn(f'Command {" ".join(cmd)} failed badly: {e}')
            raise

        log.info(f'Command {" ".join(cmd)} in {cwd} completed with {proc.returncode}')

        for line in proc.stdout.splitlines():
            log.debug(f"Output: {line}")

        if proc.returncode != 0 and check:
            raise SourceError(f"Git failed with exit code {proc.returncode}")

        return proc

    def ssh_pubkeys(self):
        """
        Yield (file, (type, key, notes)) for each public key found in <home>/.ssh/

        Note that not all of those are automatically used for git-over-ssh connections.
        """
        sshdir = self.home / ".ssh"
        for pubkey in sshdir.glob("**/id_*.pub"):
            if not pubkey.is_file():
                continue
            if pubkey.with_name(pubkey.name[:-4] + ".hidden").exists():
                continue
            yield pubkey.relative_to(sshdir), pubkey.read_text().strip().split()


class GitMirror:
    """Represents a bare clone of a remote git repository."""

    _refs: Optional[list["RefInfo"]] = None

    def __init__(self, git: GitHelper, path: Path, git_url: str):
        self.url = git_url
        self.safe_url = strip_auth(git_url)
        self.git = git
        self.path = path.absolute()
        self.timeout = 30

    def exists(self):
        return self.path.exists()

    def refs(self, fetch=False) -> Iterable["RefInfo"]:
        if not self.path.exists() or fetch:
            self.fetch()
        reflist = self.git.run(
            "git",
            "for-each-ref",
            "--sort=-creatordate",
            "--format=%(objectname) %(refname) %(creatordate:unix)",
            "refs/heads/",
            cwd=self.path,
            timeout=self.timeout,
        )
        for refline in reflist.stdout.splitlines():
            ref, name, ctime = refline.split()
            yield RefInfo(self, name, ref, float(ctime))

    def last_fetched(self) -> Optional[Datetime]:
        fhead = self.path / "FETCH_HEAD"
        if not fhead:
            return None
        return Datetime.fromtimestamp(fhead.stat().st_mtime, timezone.utc)

    def fetch(self):
        """Fetch new branches and related objects"""
        if not self.path.exists():
            log.info(f"Mirroring {self.safe_url} for the first time ...")
            self.path.parent.mkdir(parents=True, exist_ok=True)
            self.git.run(
                "git",
                "clone",
                "--bare",
                "--no-tags",
                self.url,
                self.path.name,
                cwd=self.path.parent,
                timeout=self.timeout,
            )
        else:
            log.info(f"Fetching from {self.safe_url} ...")
            self.git.run(
                "git",
                "fetch",
                "--prune",
                "-f",
                "origin",
                "refs/heads/*:refs/heads/*",
                cwd=self.path,
                timeout=self.timeout,
            )

    def getbranch(self, branch):
        return self.getref("refs/heads/" + branch)

    def getref(self, name_or_ref):
        for info in self.refs():
            if info.name == name_or_ref or info.ref == name_or_ref:
                return info

    def clone(self, target: Path):
        """Create a --shared clone in the target directory. Nothing is checked out."""
        if not self.path.exists():
            self.fetch()
        target.parent.mkdir(parents=True, exist_ok=True)
        log.info(f"Cloning mirror for {self.safe_url} to {target} ...")
        self.git.run(
            "git",
            "clone",
            "--shared",
            "--no-checkout",
            str(self.path.absolute()),
            str(target.name),
            cwd=target.parent,
            timeout=self.timeout,
        )

    def is_origin_of(self, path: Path):
        """Check if this mirror is the origin of the workdir checked out in path"""
        raise RuntimeError("Not imlemented")

    def checkout(self, target: Path, ref: "RefInfo", fix_shared=True):
        """

        Checkout a specific commit to the target direcory.

        If the mirror is not locally cached yet, it is fetched first.
        If the target directory does not exists, it is cloned first.
        The --shareed clone will share objects with the mirror directory.

        If the target exists and is not a --shared clone of the current mirror,
        the object reference will be fixed to point to the new mirror directory.
        """

        assert ref.mirror == self

        if target.exists() and fix_shared:
            altfile = target / ".git" / "objects" / "alternates"
            if not altfile.exists() or altfile.read_text() != str(self.path):
                altfile.write_text(str(self.path))

        if not target.exists():
            self.clone(target)

        self.git.run("git", "reset", "--hard", "--quiet", ref.ref, cwd=target)
        if target.joinpath(".gitmodules").exists():
            self.git.run("git", "submodule", "sync", "--recursive", cwd=target)
            self.git.run(
                "git",
                "submodule",
                "update",
                "--init",
                "--recursive",
                "--checkout",
                "--force",
                "--depth",
                "1",
                cwd=target,
                timeout=self.timeout,
            )
        self.git.run("git", "clean", "-dfxf", cwd=target)


class RefInfo:
    def __init__(self, mirror: GitMirror, name: str, ref: str, createtime: float):
        self.mirror = mirror
        self.name = name
        self.ref = ref
        self.createtime = createtime

    @property
    def is_branch(self):
        return self.name.startswith("refs/heads/")

    @property
    def branch(self):
        return self.name.replace("refs/heads/", "", 1)

    def __repr__(self) -> str:
        return f"RefInfo(name={self.name} ref={self.ref})"

    def __str__(self) -> str:
        return f"{self.ref[:8]} ({self.name})"


@dataclasses.dataclass
class SourceConfig:
    name: str
    git: str
    branch: None | str
    pattern: None | str
    dynamic: bool
    user: bool


@dataclasses.dataclass
class EnvMeta:
    name: str
    source: str


@dataclasses.dataclass
class DeployInfo:
    source: "Source"
    environemnt: str
    ref: RefInfo


class SourceRepository:
    """A persistent collection of sources and environments."""

    workdir: Path

    def __init__(self, workdir: Path):
        self.workdir = workdir

        self.lock = self._get_named_lock(f"global")
        self.git = GitHelper(home=Path.home())

    def iter_source_names(self):
        for metafile in self.workdir.glob("*.source"):
            yield metafile.stem

    def iter_sources(self):
        for name in self.iter_source_names():
            src = self.get_source(name)
            if src:
                yield src

    __iter__ = iter_sources

    def get_source(self, name):
        metafile = self.workdir / f"{name}.source"
        try:
            cfg = SourceConfig(**json.loads(metafile.read_text()))
            return Source(self, cfg)
        except FileNotFoundError:
            return None

    def update_source(self, source: "SourceConfig"):
        metafile = self.workdir / f"{source.name}.source"
        with self.lock:
            metafile.write_text(json.dumps(dataclasses.asdict(source)))

    def remove_source(self, name: str):
        metafile = self.workdir / f"{name}.source"
        with self.lock:
            metafile.unlink(missing_ok=True)
        # TODO: Delete unused mirrors
        # TODO: Delete dependant environments

    def _get_named_lock(self, name):
        return FileLock(self.workdir / f"{name}.lock")

    def get_mirror(self, git_uri: str):
        unique = sha256(git_uri.encode("utf8")).hexdigest()
        path = self.workdir / f"mirror-{unique}.git"
        return GitMirror(self.git, path, git_uri)


class Source:
    """A representation of a single source, with functions to fetch new state,
    list deployable environments, or deploy a specific environment to a
    directory.
    """

    def __init__(self, repo: SourceRepository, config: SourceConfig):
        self.config = config
        self.repo = repo
        self.lock = repo._get_named_lock(f"source-{self.name}")

        if self.config.pattern:
            self.pattern = re.compile(self.config.pattern)
        else:
            bre = re.escape(self.config.branch or "main")
            self.pattern = re.compile(f"^{bre}$")

    @cached_property
    def mirror(self):
        return self.repo.get_mirror(self.config.git)

    def match_url(self, url):
        """Return true if the source url matches the given url (ignoring auth info)"""
        return strip_auth(url) == strip_auth(self.config.git)

    @property
    def name(self):
        return self.config.name

    @property
    def is_user_defined(self):
        return self.config.user

    @property
    def is_dynamic(self):
        return self.config.dynamic

    @property
    def is_locked(self):
        return self.lock.is_locked

    def last_fetched(self):
        """Checks when the mirror was last fetched"""
        return self.mirror.last_fetched()

    def fetch(self):
        """Fetch new refs for this source now"""
        self.mirror.fetch()

    def _all_refs_filtered(self, fetch=False):
        """Collect all refs from the mirror and optionally force a fetch. May
        take a while if the mirror needs to be cloned first. Returns newest ref
        first."""
        try:
            refs = list(self.mirror.refs(fetch=fetch))
            refs.sort(key=lambda x: x.createtime, reverse=True)
            return refs
        except BaseException as e:
            raise SourceError(
                f"Failed to clone or pull from: {self.mirror.safe_url}"
            ) from e

    def _sanatize_env_name(self, name):
        return re.sub("[^a-zA-Z0-9]+", "_", name).strip("_")

    def get_envs(self, fetch=False):
        """Yield (name, ref) tuples for all environments that could be
        populated from this source."""
        refs = self._all_refs_filtered(fetch=fetch)
        refs = [ref for ref in refs if self.pattern.match(ref.branch)]

        # TODO: Allow and branch name -> env name transforms
        if self.is_dynamic:
            for ref in refs:
                name = self._sanatize_env_name(ref.branch)
                yield name, ref
        elif refs:
            # Just the newest one
            yield self.name, refs[0]
