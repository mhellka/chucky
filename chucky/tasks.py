from abc import abstractmethod
from concurrent.futures import Future, InvalidStateError, ThreadPoolExecutor
import json
import logging
from pathlib import Path
import threading
from typing import Any, Optional
import uuid
import time
import atexit

log = logging.getLogger(__name__)


class TaskRunner:
    timeout = 60 * 60

    def __init__(self, workdir: Path):
        self._cleanup_lock = threading.RLock()
        self._shutdown = False
        self.workdir = workdir.absolute()
        self.pool = ThreadPoolExecutor(max_workers=4)
        self.local_tasks = {}
        self.workdir.mkdir(parents=True, exist_ok=True)

        # Remove task files created by other (crashed?) processes.
        for old in self.workdir.glob("*.json"):
            try:
                if old.stat().st_mtime < time.time() - self.timeout * 2:
                    old.unlink(missing_ok=True)
            except (OSError, IOError):
                pass
        atexit.register(self.shutdown)

    def task_info(self, id):
        taskfile = self.workdir / f"{id}.json"
        if not taskfile.exists():
            return None
        meta = json.loads(taskfile.read_text())
        return meta

    def shutdown(self):
        if self._shutdown:
            return
        self._shutdown = True
        with self._cleanup_lock:
            for task in list(self.local_tasks.values()):
                task._forget()
            self.local_tasks.clear()
            self.pool.shutdown()

    def cleanup(self):
        if self._shutdown:
            return

        if not self._cleanup_lock.acquire(blocking=False):
            return  # In progress
        try:
            # Forget tasks finished more than an hour ago
            maxage = time.time() - self.timeout
            for task in list(self.local_tasks.values()):
                if 0 <= task.ts_end < maxage:
                    task._forget()
                    self.local_tasks.pop(task.id)
        finally:
            self._cleanup_lock.release()

    def _submit(self, task: "BaseTask") -> Future[Any]:
        if self._shutdown:
            raise InvalidStateError("Pool shutdown")
        self.cleanup()
        log.info(f"New task: {task}")
        return self.pool.submit(task.run)


class BaseTask:
    def __init__(self, runner: TaskRunner):
        self.id = str(uuid.uuid4())
        self.runner = runner
        self._future: Optional[Future] = None
        self._lock = threading.RLock()
        self.subtasks = []
        self.tasklog = []

        self.ts_start = -1
        self.ts_end = -1

    def _persist(self):
        if self.runner._shutdown:
            return
        with self._lock:
            metafile = self.runner.workdir / f"{self.id}.json"
            payload = {
                "id": self.id,
                "name": self.__class__.__name__,
                "started": self.ts_start,
                "completed": self.ts_end,
                "result": None,
                "error": None,
                "log": self.tasklog,
            }
            if self._future and self._future.done():
                try:
                    payload["result"] = self._future.result()
                    if payload["result"] is None:
                        payload["result"] = True
                except Exception as e:
                    payload["error"] = str(e)

            log.info(f"Persisting: {payload}")

            metafile.write_text(json.dumps(payload))

    def _forget(self):
        metafile = self.runner.workdir / f"{self.id}.json"
        metafile.unlink(missing_ok=True)
        if self._future:
            self._future.cancel()

    def wait(self, timeout: Optional[float] = None):
        if not self._future:
            raise InvalidStateError("Task not submitted")
        try:
            self._future.result(timeout)
            return True
        except TimeoutError:
            return False
        except:
            return True

    def result(self, timeout=None):
        if not self._future:
            raise InvalidStateError("Task not submitted")
        return self._future.result(timeout)

    def submit(self) -> Future[Any]:
        """Submit this task to a thread pool (if not already submitted) and return its future."""
        with self._lock:
            if self._future:
                return self._future
            self._persist()
            self._future = self.runner._submit(self)

            @self._future.add_done_callback
            def on_complete(f):
                self.ts_end = time.time()
                self._persist()

        return self._future

    def log(self, msg, level=logging.INFO, ts: float = 0.0):
        """Log messages targeted at end users"""
        msg = msg.rstrip()
        log.log(level, f"{self.__class__.__name__} [{self.id}]: {msg}")
        self.tasklog.append((ts or time.time(), logging.getLevelName(level), msg))

    def run(self):
        """Run this task syncronously"""
        if self.runner._shutdown:
            raise InvalidStateError("Pool shutdown")
        with self._lock:
            if self.ts_start > -1:
                raise RuntimeError("Task started twice")
            self.ts_start = time.time()

        self.log(f"Task started: {self!r}", ts=self.ts_start)
        self._persist()

        try:
            result = self._run()
            self.ts_end = time.time()
            self.log(f"Task completed", ts=self.ts_end)
            return result
        except Exception as e:
            self.ts_end = time.time()
            self.log(f"Task failed: {e!s}", logging.ERROR, ts=self.ts_end)
            raise

    @abstractmethod
    def _run(self) -> Any:
        pass
