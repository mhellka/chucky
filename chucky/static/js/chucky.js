
document.querySelectorAll('[data-bs-toggle="tooltip"]').forEach(el => {
  new bootstrap.Tooltip(el)
})

function copyToClipboard(key) {
  if (navigator.clipboard && navigator.clipboard.writeText) {
    navigator.clipboard.writeText(key)
  } else {
    alert("Missing browser support for clipboard")
  }
}

function alertError(r) {
  if (r.status < 400)
    return Promise.resolve(); // No error

  return r.json().then(json => {
    alert(json.error + ": " + json.message)
    console.log("API Error", json)
  }, e => {
    alert("Error: " + r.status)
  })
}

function backdrop(enable, text) {
  const bd = document.getElementById("backdrop")
  if (enable) {
    bd.getElementsByTagName("strong")[0].innerHTML = text || "Loading..."
    bd.classList.add("show");
  } else {
    bd.classList.remove("show");
  }
}

function makeSortable(th) {
  console.log(th)
  if(th.tagName === 'TH' && !th.classList.contains("tsort")) {
    th.classList.add("tsort")
    th.addEventListener("click", e => { sortRow(e.target) })
  }
}

function sortRow(th) {
  /* Accepts a <th> dom element and sorts the table by that column. If called
     again, reverts sort order.

     We assume that the table has only one <tbody> and that all rows have the
     same cell count (no merged cells).

     Rows are sorted by the trimmed text content of each cell, or by the
     `data-tsort` attribute if present. Numeric sort mode is on by default,
     which means that "x2x" is considered smaller than "x10x".

     The sorted <th> element is decorated with either `tsort-asc` or
     `tsort-desc` class. Those classes are removed from all other <th>
     elements.

     The <tr> nodes are moved around in-place and not cloned or re-created.
     This is not very efficient for large tables because it triggers a ton
     of re-draws, but it helps prevent flickering and keeps event handlers
     alive.
  */

  const table = th.closest('table')
  const tbody = table.tBodies[0] // ignore all but the first one
  const index = Array.prototype.indexOf.call(th.parentNode.children, th);
  const order = th.classList.contains("tsort-asc") ? -1 : 1;

  table.querySelectorAll("th").forEach(node =>
    node.classList.remove("tsort-asc", "tsort-desc")
  )
  th.classList.add("tsort-" + (order < 0 ? 'desc' : 'asc'))

  Array.from(tbody.children).map(row => {
    const cell = row.children[index] || row
    const value = (cell.dataset.tsort || cell.textContent || "").trim()
    return { row, value }
  }).sort((a, b) => {
    return a.value.localeCompare(b.value, undefined, { numeric: true }) * order;
  }).forEach(row => {
    tbody.appendChild(row.row);
  });
}

/**
 * Poll task ID 
 */
async function pollTask(id) {
  const rs = await fetch(`/api/v1/task/${id}`)
  if (rs.status == 404)
    return;
  return await rs.json()
}


async function sleep(ms) {
  await new Promise(r => setTimeout(r, ms))
}


async function triggerDeploy(source, environment, force) {

  let formData = new FormData();
  formData.append('source', source);
  if (environment)
    formData.append('environment', environment);
  if (force)
    formData.append('force', "true");

  try {
    backdrop(true, "Fetching git sources ...");

    const rs = await fetch("/api/v1/deploy", {
      method: "post",
      body: formData
    })

    if (rs.status >= 400)
      await alertError(rs)

    const info = await rs.json()
    const tasks = info.tasks || [];
    
    if (tasks.length > 0) {
      var done = 0;
      backdrop(true, `Deploying environments [${done}/${tasks.length}] ...`);

      const waitLoops = tasks.map(async (task) => {
        var loops = 0;
        while (++loops < 100) {
          await sleep(300 + 100 * loops * (1 + Math.random()));
          const taskResult = await pollTask(task.id)
          if (taskResult && taskResult.completed > -1) {
            done += 1;
            backdrop(true, `Deploying environments [${done}/${tasks.length}] ...`);
            break
          }
        }
      })

      await Promise.all(waitLoops)
    }

    window.location.reload();
  } catch (e) {
    alert(e)
  }
}

function triggerUndeploy(environment) {
  backdrop(true, "Removing environment ...");
  let formData = new FormData();
  formData.append('environment', environment);
  fetch("/api/v1/destroy", {
    method: "post",
    body: formData
  }).catch(e => alert(e)).then(async r => {
    if (r.status >= 400)
      await alertError(r)
    window.location.reload();
  })
}