import argparse

if __name__ == "__main__":
    from chucky import Chucky

    _chucky = Chucky()

    parser = argparse.ArgumentParser()
    parser.add_argument("sources", metavar="NAME", nargs="*", help="Sources to trigger")
    parser.add_argument(
        "--force", action="store_true", help="Re-deploy even if nothing changed"
    )

    args = parser.parse_args()
    names = args.sources or list(_chucky.sources.iter_source_names())
    force = False
    tasks = []
    print("Starting tasks ...")
    for name in names:
        source = _chucky.sources.get_source(name)
        if not source:
            continue
        tasks.extend(_chucky.deployer.sync(source, force=args.force).values())
    print("Waiting for tasks to finish ...")
    for task in tasks:
        task.future.result()
