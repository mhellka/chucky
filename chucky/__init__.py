import logging
import sys
import os
import chucky.config
import chucky.sidecar
import chucky.deploy
import chucky.sources
import chucky.app
import chucky.tasks


class Chucky:
    def __init__(self, setup_logging=True):
        self._config = None
        self._sidecar = None
        self._deployer = None
        self._runner = None
        self._sources = None
        self._app = None

        if setup_logging:
            logging.basicConfig(
                stream=sys.stdout,
                level=os.environ.get("CHUCKY_LOGLEVEL", "DEBUG"),
                format="%(asctime)s [%(thread)d] %(levelname)-8s %(name)s - %(message)s",
            )

    @property
    def config(self) -> chucky.config.ChuckyConfig:
        if not self._config:
            self._config = chucky.config.ChuckyConfig()
            self._config.load(env_prefix="CHUCKY_")
        return self._config

    @property
    def sidecar(self) -> chucky.sidecar.Sidecar:
        if not self._sidecar:
            self._sidecar = chucky.sidecar.Sidecar(
                self.config.sidecar, self.config.protected_certs
            )
        return self._sidecar

    @property
    def runner(self) -> chucky.tasks.TaskRunner:
        if not self._runner:
            taskdir = self.config.workdir / "tasks"
            self._runner = chucky.tasks.TaskRunner(taskdir)
        return self._runner

    @property
    def deployer(self) -> chucky.deploy.DeployService:
        if not self._deployer:
            deploydir = self.config.workdir / "environments"
            self._deployer = chucky.deploy.DeployService(
                deploydir, self.sidecar, self.runner
            )
        return self._deployer

    @property
    def sources(self) -> chucky.sources.SourceRepository:
        if not self._sources:
            sourcedir = self.config.workdir / "sources"
            self._sources = chucky.sources.SourceRepository(sourcedir)
            self._enforce_sources_config(self._sources, self.config.sources)
        return self._sources

    def _enforce_sources_config(self, sources, source_configs, remove_orphaned=True):
        loaded = set()
        for name, conf in source_configs.items():
            if "branch" in conf:
                branch, pattern = conf["branch"], None
            elif "pattern" in conf:
                branch, pattern = None, conf["pattern"]
            else:
                branch, pattern = None, ".*"

            cfg = chucky.sources.SourceConfig(
                name=name,
                git=conf["git"],
                branch=branch,
                pattern=pattern,
                dynamic=conf.get("dynamic", False),
                user=False,
            )
            loaded.add(cfg.name)
            sources.update_source(cfg)

        # Remove all non-user-defined sources not loaded during this iteration
        if remove_orphaned:
            for source in list(sources):
                if source.is_user_defined:
                    continue
                if source.name in loaded:
                    continue
                sources.remove_source(source.name)

    @property
    def app(self):
        if not self._app:
            self._app = chucky.app.bootstrap(self)
        return self._app


def wsgi():
    return Chucky().app
