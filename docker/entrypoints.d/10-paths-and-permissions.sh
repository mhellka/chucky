#!/bin/bash
set -e

test -d "$CHUCKY_WORKDIR" || mkdir -p "$CHUCKY_WORKDIR"

chown -R chucky:chucky /home/chucky/
chown -R chucky:chucky "$CHUCKY_WORKDIR"
chmod -R u=Xrw,g=Xr,o=Xr "$CHUCKY_WORKDIR"
