#!/bin/bash
set -e

# Fix variables
export CHUCKY_WORKDIR="${CHUCKY_WORKDIR:-/home/chucky/work}"

# Run entrypoints
run-parts --regex '[a-zA-Z0-9_.]+' /entrypoints.d/

# Run the actual command
exec runuser -u chucky -- "$@"

