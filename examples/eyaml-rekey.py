#!/usr/bin/env python3
"""
  Re-encrypt all encrypted values in a yaml file with a new eyaml key.

  You usually need to do this after rotating yaml keys on the puppet server.

  This script can use either with a locally installed `eyaml` executable and
  local key files, or a remote chucky service with access to a sidecar running
  on the actual puppet server. The remote option is useful if you do not have
  (or want to provide) access to private eyaml keys, or do not want to install
  eyaml locally. For this to work, the chucky sidecar must implement the
  `eyaml-rekey` command and have access to both the old and new keypairs. Chucky
  then makes this functionality available via the /api/v1/secrets/rekey endpoint
  and allows scripts like this one to re-key secrets without access to prvate key
  or plaintext values.

"""

import os
import sys
import re
import argparse
from pathlib import Path
from typing import Callable, Optional
from subprocess import run, PIPE
import urllib.request
import urllib.parse


def main():
    parser = argparse.ArgumentParser(prog=sys.argv[0])
    exclusive = parser.add_mutually_exclusive_group(required=True)
    exclusive.add_argument(
        "--chucky",
        metavar="API",
        help="Chucky URL. If login is required, proivde a valid session token via CHUCKY_TOKEN environment variable.",
    )
    exclusive.add_argument(
        "--keypath",
        metavar="PATH",
        help="Path to old keys and new keys. The directory should contain the new key pair (private_key.pkcs7.pem and public_key.pkcs7.pem) and any number of old keypairs in subdirectories.",
    )
    parser.add_argument(
        "files",
        metavar="FILE",
        nargs="*",
        help="File(s) to re-key. The file is updated in-place! If not specified, read from stdin and write to stdout.",
    )
    args = parser.parse_args()

    if args.chucky:
        rekey = lambda data: rekey_chucky(data, args.chucky)
    else:
        rekey = lambda data: rekey_local(data, args.keypath)

    try:
        if not args.files or args.files == ["-"]:
            text = sys.stdin.read()
            print(replace(text, rekey, "STDIN"), end="")
        else:
            for path in args.files:
                text = Path(path).read_text()
                Path(path).write_text(replace(text, rekey, str(path)))
    except ValueError as e:
        print(*e.args, file=sys.stderr)
        sys.exit(1)


def mkerror(match, secret, text, filename):
    lines = text[: match.start()].splitlines()
    lineno = len(lines)
    column = len(lines[-1])
    return ValueError(
        f"Failed to decode secret in {filename} (line {lineno} offset {column}): {secret}"
    )


def fix_indent(text, template):
    """Break text into multiple lines and guess correct indention and maximum line length from template."""
    tpl_lines = template.splitlines(True)
    maxlen = max(len(line.strip()) for line in tpl_lines)
    lines = []
    for i in range(0, len(text), maxlen):
        line = text[i : i + maxlen]
        refline = tpl_lines[i] if len(tpl_lines) > i else tpl_lines[-1]
        indent = refline[: len(refline) - len(refline.lstrip())]
        lines.append(indent + line)
    return "\n".join(lines)


def replace(text, rekey: Callable[[str], Optional[str]], filename):
    def sub(match):
        orig = match.group(0)
        compact = "".join(orig.split())
        rekeyed = rekey(compact)
        if not rekeyed:
            raise mkerror(match, compact, text, filename)
        if "\n" in orig:
            rekeyed = fix_indent(rekeyed, orig)
        return rekeyed

    return re.sub(r"ENC\[[^\]]+\]", sub, text)


def rekey_chucky(secret, api):
    api = api.rstrip("/")
    if "://" not in api:
        api = f"https://{api}"
    rekey_url = f"{api}/api/v1/secrets/rekey"

    headers = {}
    headers["Content-Type"] = "application/x-www-form-urlencoded"
    if "CHUCKY_TOKEN" in os.environ:
        headers["Cookie"] = f'chucky-session={os.environ["CHUCKY_TOKEN"].strip()}'

    data = urllib.parse.urlencode({"secret": secret}).encode("utf8")
    rq = urllib.request.Request(rekey_url, method="POST", data=data, headers=headers)
    with urllib.request.urlopen(rq) as rs:
        result = rs.read().decode("utf8").strip()
        if rs.status != 200 or not result.startswith("ENC["):
            print(
                f"Unexpected chucky response: {rs.status} {result}",
                file=sys.stderr,
            )
            return
        return result


def rekey_local(secret, keypath):
    plaintext = None
    # Try all keys (including current key) to decode secret
    for pubkey in Path(keypath).glob("**/public_key.pkcs7.pem"):
        privkey = pubkey.with_name("private_key.pkcs7.pem")
        if not privkey.exists():
            continue  # Missing privkey
        cmd = ["eyaml", "ecrypt", "--stdin"]
        cmd += ["--pkcs7-private-key", privkey]
        cmd += ["--pkcs7-public-key", pubkey]
        proc = run(cmd, input=secret, encoding="latin1", stdout=PIPE)
        if proc.returncode != 0:
            continue  # Wrong key
        plaintext = proc.stdout[:-1]  # eyaml adds a tailing newline
        break

    pubkey = Path(keypath) / "public_key.pkcs7.pem"

    if plaintext:
        cmd = ["eyaml", "encrypt", "--stdin", "--output", "string"]
        cmd += ["--pkcs7-public-key", Path(keypath) / "public_key.pkcs7.pem"]
        proc = run(cmd, input=plaintext, encoding="latin1", stdout=PIPE)
        if proc.returncode == 0 and "ENC[" in proc.stdout:
            return proc.stdout.strip()

    return


if __name__ == "__main__":
    main()
