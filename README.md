# Chucky - A control panel for Puppet servers 

Chucky is a web application to simplifying or automate common tasks usually done via console commands directly on a puppet-server. It enables teams or groups to easily manage client certificates, deploy environments from git or configure deployment triggers via webhooks.

Features:

* Sign, revoke or delete puppet client certificates.
* Deploy puppet environments from git version control.
* Trigger deployments manually or integrate chucky with github/gitlab webhooks.
* Encrypt eyaml secrets via browser UI or API.
* Query PuppetDB with [PQL](https://www.puppet.com/docs/puppetdb/7/api/query/examples-pql.html).
* Browse your inventory with [Puppetboard](https://github.com/voxpupuli/puppetboard) (included in chucky).
* Authenticate users via OpenID Connect and limit permissions with role based access control (optional).

## Configuration

Chucky reads configuration from a yaml file as well as environment variables named `CHUCKY_*` (all uppercase). Config values from the yaml file have preference over environment variables. Lists are usually comma-separated if defined as an environment variable.

* `CHUCKY_CONF` Path to the config yaml file. (default: `/etc/chucky/config.yaml`)
* `workdir` (required) Path to a work directory where git repositories are checked out to and other runtime files are stored.
* `secret` (required) Base secret used to generate other secrets e.g. `webhook_secret`
* `webhook_secret` API secret for webhooks (default: derive from `secret`)
* `public_url` Full public url (including scheme and tailing slash) for this service. (default: `/`)
* `links` List of links to display in the header of the admin dashboard. Each link should have a title and an url, separated by space.
* `puppetserver` Domain name (and port) of the puppet server. Optional, just used to display a help text in UI.
* `puppetdb` Full url (e.g. https://domain:port/") for puppetdb. Optional, but needed to enable puppetboard.
* `protected_certs` List of certnames that should not be revoke- or removable via UI (default: `puppet,puppetdb`).
* `sidecar` Command to execute sidecar actions on the puppet server. Chucky does not directly call puppet or puppetserver commands, but passes simplified commands to a sidecar script that runs either locally or on a remote puppet server and does the actual work. If chucky runs with lowered privileges, you can use `sudo` here to trigger sidecar actions as a privileged puppet user. Sidecar actions on a different server can be triggered via ssh. See `sidecar/README.md` for details and more examples.
* `oidc_client` If configured, this enables authentication against an OpenID Connect identity provider.
  * `oidc_secret` (required) Client secret.
  * `oidc_config_uri` (required) OpenID configuration URI (e.g. `https://keycloak.example.com/realms/my-realm/.well-known/openid-configuration`)
  * `oidc_scopes` Client scopes, separated by space (default: `openid`).
  * `oidc_redirect_uri` OpenID redirect_uri. (default: `/oidc`)
  * `oidc_id_claim` Claim for the unique account ID (default: `sub`). See `oidc_id_mapping`.
  * `oidc_name_claim` Claim for an account display name (default: `name`)
  * `oidc_role_claim` Claim or list of claims used to assign roles to users (default: `roles`). See `oidc_role_mapping`. 
  * `oidc_role_mapping` A list of `oidcRole=chuckyRole` mappings to translate roles that came from OIDC claims to roles understood by chucky. The mapping rules are applied in order, which allows to translate each OIDC-role to multiple chucky-roles if needed. OIDC-roles without a mapping are ignored. The empty OIDC-role is applied to all authenticated users, even if the role-claim is empty or missing. So, in order to grant everyone admin privileges, simply configure `=admin`. If not configured at all (default), roles starting with `chucky_` will be translated to chucky roles.
  * `oidc_id_mapping` A list of `accountId=chuckyRole` mappings to add additional chucky roles to specific accounts. This is useful if your OIDC provider does not provide meaningful roles or groups on its own.

## Authentication and Authorisation

Chucky assumes that it runs either in a protected environment with no authentication at all (anyone can do anything) or in an enterprise environment with an OpenID Connect provider (e.g. Keycloak). Authorization works via OAuth claims mapped to chucky-specific roles. Two claims are involved here, `oidc_id_claim` (default: `sub`) and `oidc_roles_claim` (default: `roles`). These can be mapped to chucky roles via `oidc_id_mapping` (for individual users) and `oidc_role_mapping` (for user groups). The following roles are implemented:

  * `info` can view the dashboard. This is the most basic role.
  * `cert` can sign, revoke or delete certificates. Implies `info`.
  * `deploy` can trigger deployments. Implies `info`.
  * `eyaml` an encode secrets. Implies `info`.
  * `board` can view puppetboard and query puppetdb. Implies `info`.
  * `dev` implies `info,cert,deploy,eyaml,board`.
  * `admin` can do everything.

### A word on environments, secrets and security

There is a not very well documented pitfall with puppet that has major implications on security: Puppet modules are code. They are executed on the puppet server with all permissions needed to read (and sometimes write) configuration, code and data from all environments, including eyaml private keys. This also applies to modules pulled in from external sources (e.g. puppetforge). Anyone allowed to push code to a puppet server can easily decode eyaml secrets, extract private keys or run arbitrary commands as the `puppet` user. External node classifiers (ENCs) often used to force nodes into specific environments do not prevent this kind of attack. There is virtually no isolation between environments.

For this reason, Chucky does not even try to separate user permissions based on environments. If you feel the need to give only some users access to certain environments, think about deploying multiple puppet server instances instead.

## Sources

Sources are listed in the configuration yaml file and define how to deploy environments from git version control. Static sources have a name, a `git` url and either a `branch` or regular expression `pattern`. In case of a pattern, the most recently updated branch matching the pattern is used for that environment. If all matching branches disappears from git, then the environment will also disappear.

```yaml
sources:
  production:
    git: https://example.com/puppet.git
    branch: main
  development:
    git: https://example.com/puppet.git
    pattern: "^dev_.*$"
```

Sources with a `dynamic` flag are handled differently: These automatically create or destroy dynamic environments based on branches matching a specific pattern. Each matching branch will spawn a new environment with the same name. Deleting a branch also destroys the corresponding environment.

```yaml
sources:
  dynamic_env:
    git: https://example.com/puppet.git
    pattern: "^dev_.*$" # defaults to all branches
    dynamic: True
```

Environments belong to the source they originated from. If, for example, a dynamic source detects a new branch that has the same name as an already existing environment created from a different source, then nothing happens. The existing environment is not changed. This ensures that environments would not uncontrollably 'flap' between two sources if there are naming conflicts.

### SSH keys and `git@` URLs

Cloning or fetching from `git@` URIs requires `ssh` and a password-less key pair in `~/.ssh/` to work properly. Note that SSH will only pick up keys that have certain names, or have matching `IdentityFile` entries in `~/.ssh/config`. If you are running chucky as a docker container, most of the work is done for you. See below.

The UI will display all `id_*.pub` files it can find in `~/.ssh/` or its subdirectories. You can hide a key from UI by removing its public key, or by adding a matching `id_*.hidden` file. They are still used for SSH connections or git clone/fetch operations if configured properly in `~/.ssh/config`.

### Credentials and `https://` URLs

Chucky will use credentials provided as part of `https://` git urls when cloning or fetching from a private git repository. This is often easier than configuring ssh keys, and secure enough when using read-only tokens instead of actual user credentials. These credentials are not shown in the web UI and webhooks will match sources regardless of credentials.

## Webhooks and automated deployments (CI/CD)

Chucky deployments can be triggered from gitlab webhooks or CI/CD pipelines, allowing fully automated deployments and a git centric workflow. Support for github and custom webhooks is also planned. To trigger deployments, send an HTTPS POST request to `/githook` or `/githook/<source>` with event metadata as `application/json` and a security token matching the one configured in `webhook_secret`. The `/githook` endpoint will try to guess the affected sources from the event metadata by comparing git URLs. The `/githook/<source>` endpoint will always trigger deployments for the named source and ignore the request body. The `<source>` part can be a comma-separated list of multiple source names if you want to trigger multiple deployments with a single webhook event.

If your favourite CI/CD environemnt is not supported yet, you can simply send a request that looks like a gitlab push event webhook. The request should have `X-Gitlab-Event: Push Hook` and `X-Gitlab-Token: <webhook_secret as plaintext>` headers and a request body containing a minimal `application/json` document. Chucky looks at the `repository.git_http_url` and `repository.git_ssh_url` fields to auto-detect sources to deploy.

## Chucky Docker Container

The chucky docker container performs a bunch of operation on startup to make operation easier. Namely:

* Automatically generate `/home/chucky/.ssh/id_ed25519` and `/home/chucky/.ssh/id_ed25519.pub`, if missing.
* Search for `id_*` files in `/etc/chucky/ssh/` (including subdirectories) and copy them over to `/home/chucky/.ssh/keys/` with the correct permissions.
* Generate `/home/chucky/.ssh/config` with one `IdentityFile` entry per `id_*` files found in `/home/chucky/.ssh/` or its subdirectories. Warning, git servers will accept the first known key and ignore all the others. Do not used multiple keys for the same git server.
* Append `/etc/chucky/ssh/config` to `/home/chucky/.ssh/config`, if present.
* Fixing file and directory permissions if needed.

You may want to mount `/home/chucky/` as a persistent volume, but be careful: Chucky will fix permissions to match the internal chucky user on startup, so DO NOT mount your actual home or `~/.ssh/` folder into the container. Mount a directory with custom keys to `/etc/chucky/ssh/` instead.
