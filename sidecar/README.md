# Chucky Sidecar

Chucky needs to execute certain commands with puppet server privileges on the puppet server itself. This poses some difficulties: Giving Chucky (or any web application) too much power is usually not a good idea. Chucky may hurt people if not contained properly, you know, like in the movies. You may also want to run Chucky on a separate server or in a different container than puppet itself. For this reason, Chucky will not directly call puppet or puppetserver commands, but pass simplified commands to a single sidecar script that runs either locally or on a remote puppet server and does all the actual work.

A fully functional example sidecar script named `chucky.sh` can be found in this repo. You can just copy that to a puppet server, or extend it or write your own. It needs to be run as the `puppet` user though. If Chucky runs on the same host as puppet server, you can use `sudoers` rules to allow the `chucky` user to run `sudo -u puppet /path/to/chucky.sh` but nothing else. If the puppet server runs on a different host, you can do the same via SSHs `ForceCommand` option or the`command=` parameter in an  `authorized_keys` file. Both approaches are described below.

The provided `chucky.sh` will read some configuration from `/etc/defaults/chucky` if that file is present. But the default should work most of the time.

## Sidecar commands

If you choose to write your own sidecar script or extend `chucky.sh`, it should implement the following functionality and exit with status code `0` on success, or any other status code on errors.

* `caps` Print the sidecar api version (currently `v0`) followed by a space-separated list of supported commands. This command is mandatory, all others are optional. Example: `v0 caps health deploy undeploy ca-list ca-sign` and so on.
* `health` Check local services and return a non-zero status code if something is wrong (e.g. puppet server not responsive, puppetdb offline, disk full). The first line of output should contain some human readable status message.
* `deploy <name>` Deploy a specific puppet environment. Chucky will pack the environment folder into an uncompressed tar archive and pipe it to the sidecar command via standard input. The sidecar should read the tar archive from standard input, extract it into a temporary folder, perform checks and deploy tasks (e.g. check syntax, resolve dependencies from `Puppetfile` if present) and finally copy the result to `/etc/puppetlabs/code/<name>/`. \
All output (stderr and stdout) is logged to help users solving dependency conflicts or other deploy errors. Make sure no secrets are printed during this step.
* `undeploy <name>` Undeploy a specific environment. The command should delete the named environment folder.
* `ca-list` List all certificates as a tab-separated list with exactly 4 columns: certname, state, fingerprint and info. The 'state' value should be either `requested`, `signed` or `revoked`. Fingerprint and info columns are free-form, but should be human readable and not contain tab characters. The info field may contain additional info such as alt_names or certificate extension fields.
* `ca-sign <certname>` Sign a single certificate by name.
* `ca-revoke <certname>` Revoke a single certificate by name.
* `ca-clean <certname>` Completely remove a single certificate by name.
* `eyaml-encrypt` Encrypt standard input with the current public key and return the encrypted string. This should behave like `eyaml encrypt --output string --stdin`.
* `eyaml-key` Return the current public key.
* `eyaml-rekey` Read an encrypted secret from standard input, try to decode it with all keys found on disk, re-encrypt it with the current key and return the result. If the input decodes with the current key, it should be returned as is (no re-encryption necessary). If none of the keys was able to decrypt the given input, return an error. This can be used to re-encrypt existing secrets after rotating eyaml keys without disclosing secrets or private keys to the user.

## Securing Chucky sidecar execution

The example `chucky.sh` script assumes to be run by the `puppet` user on a puppet server and implements all required commands. There are several ways to allow Chucky to run that script without giving chucky too much privileges, some of which are described here.

### Sudoers

If Chucky runs on the same host as puppetserver, you can allow the `chucky` user to run `sudo -u puppet /path/to/chucky.sh`, but nothing else. The sudoers file would look like this (assuming Chucky runs as the user `chucky`):

```
chucky ALL=(puppet) NOPASSWD: /path/to/chucky.sh
```

Chucky would be configured to run `chucky.sh` subcommands via sudo:

```
sidecar: sudo -u puppet /path/to/chucky.sh
```

### SSH

If Chucky runs on a different server or in a separate container, then SSH may be the easiest and most secure option to trigger the sidecar script. This requires an ssh server running on the puppetserver, and some configuration to allow chucky to connect to your puppetserver as the `puppet` user and execute the sidecar script remotely. To avoid a password prompt and make the connection more secure, generate an SSH key on the chucky host and add the public key to the `~puppet/.ssh/authorized_keys` file on the puppetserver. We can further lock down chuckys access on the puppetserver by disabling unneeded SSH features (e.g. port forwarding) and restricting the command it is allowed to execute. A nice and secure `~puppet/.ssh/authorized_keys` file would look like this:

```
# ~puppet/.ssh/authorized_keys
restrict,command="/path/to/chucky.sh" <public-ssh-key>
```

Note that the `puppet` user may be a system user and not have its home-folder at the usual place. For example, within the official `puppet/puppetserver` docker image, `~puppet` resolves to `/opt/puppetlabs/server/data/puppetserver`. The puppet user may also need a shell or the sshd daemon will deny logins, even if `~puppet/.ssh/authorized_keys` exists.

`chucky.sh` is smart enough to detect execution via SSH and should work as expected out of the box. The Chucky sidecar configuration would look like this:

```
sidecar: ssh -i /path/to/id_key root@puppetserver
```

To drastically speed up SSH connections you may want to enable compression and connection multiplexing on the client (the chucky host). The following `~chucky/.ssh/config` will tell `ssh` to keep the connection open and allow other `ssh` calls to re-use the same connections, skipping the handshake and authentication phase and shaving of more than 90% of the connection overhead per call:

```
Host YOUR.PUPPET.HOST
    ServerAliveInterval 60
    ControlMaster auto
    ControlPersist 10m
    ControlPath ~/.ssh/conn-%C
    Compression yes
```