#!/bin/bash
set -e
set -o pipefail
shopt -s globstar

# This script implements all commands required by chucky in a single bash script.
# It assumes to be run as the puppet user on the puppet server.
# Openssh authozired_keys 'command' jails are supported (and recommended) when
# triggering this script via SSH.

CODEDIR=/etc/puppetlabs/code
SSLDIR=/etc/puppetlabs/puppet/ssl
EYAML_KEYDIR=/etc/puppetlabs/puppet/eyaml
EYAML_PUBKEY=$EYAML_KEYDIR/public_key.pkcs7.pem
EYAML_ALLKEYS=$EYAML_KEYDIR/**/public_key.pkcs7.pem
PUPPET_MASTER="${HOSTNAME}:8140"
CONFDIRFIX=true
PATH="$PATH:/opt/puppetlabs/bin/:/opt/puppetlabs/puppet/bin/"
CAPS="v0 health deploy undeploy ca-list ca-sign ca-revoke ca-clean eyaml-encrypt eyaml-key eyaml-rekey"
CACHE="$HOME/.cache/"

# You can override config in /etc/defaults/chucky
test -r /etc/defaults/chucky && source /etc/defaults/chucky

main() {
    test "$EUID" -ne 0 || die "Do not run this as root!"
    test -d "$CODEDIR" || die "Codedir is not a directory: $CODEDIR"
    test -O "$CODEDIR" || die "Current user $(id) is not the owner of $CODEDIR"

    CMD="$1"
    shift

    case "$CMD" in
    "caps")
        caps
        ;;
    "health")
        health
        ;;
    "deploy")
        deploy "$@"
        ;;
    "undeploy")
        undeploy "$@"
        ;;
    "ca-list")
        ca_list
        ;;
    "ca-sign")
        ca_sign "$@"
        ;;
    "ca-revoke")
        ca_revoke "$@"
        ;;
    "ca-clean")
        ca_clean "$@"
        ;;
    "eyaml-encrypt")
        eyaml_encrypt
        ;;
    "eyaml-key")
        eyaml_key
        ;;
    "eyaml-rekey")
        eyaml_rekey
        ;;
    *)
        die "Unknown command: $CMD"
        ;;
    esac
}


die() {
    echo "$@" 1>&2
    exit 1
}

say() {
    echo ">>>" "$@"
}


_fixconfdir() {
    # The 'puppetserver' tool expects config to be in ~/.puppetlabs/etc/puppet
    # when run as non-root and does not support the --confir parameter.
    # We can fix that with a simple symlink. Not pretty, but better than running
    # everything as root all the time.
    test ! -e ~/.puppetlabs/etc/puppet || return 0
    test "$CONFDIRFIX" = "true" || return 1

    mkdir -p ~/.puppetlabs/etc/
    ln -s /etc/puppetlabs/puppet ~/.puppetlabs/etc/puppet
}



caps() {
    echo "$CAPS"
}


health() {
    CERTNAME=$(cd "$SSLDIR/certs" && ls *.pem | grep --invert-match ca.pem | head -n1)
    curl --fail \
        --silent \
        --max-time 5 \
        --cert    "$SSLDIR/certs/$CERTNAME" \
        --key     "$SSLDIR/private_keys/$CERTNAME" \
        --cacert  "$SSLDIR/certs/ca.pem" \
        "https://$PUPPET_MASTER/status/v1/simple" \
        | grep -q '^running$' \
        || die "DOWN: Api $PUPPET_MASTER not responsive"
    echo "OK"
}

deploy() {
    ENVNAME="$1"
    [[ "$ENVNAME" =~ ^[a-zA-Z0-9_-]+$ ]] || die "Bad environment name"

    WORKDIR="$(mktemp -d)"
    trap 'test -d "$WORKDIR" && rm -rf -- "$WORKDIR"' EXIT

    # Extract environment to workdir
    say "Extracting environment to $WORKDIR ..."
    mkdir -p "$WORKDIR/environments/"
    tar -C "$WORKDIR/environments/" -xv --exclude-vcs <&0 \
      || die "Failed to extract TAR stream (code $?)"
    test -d "$WORKDIR/environments/$ENVNAME" \
      || die "TAR stream did not contain $ENVNAME folder"

    # Run deployment actions in workdir
    cd "$WORKDIR/environments/$ENVNAME"
    for PUPPETFILE in "Puppetfile_$ENVNAME" "Puppetfile"; do
      if [[ -f "$PUPPETFILE" ]]; then
        export GIT_SSH_COMMAND="ssh -o StrictHostKeyChecking=accept-new"
        export LIBRARIAN_PUPPET_TMP="$CACHE/chucky.librarian/$ENVNAME"
        if egrep -q '^\s*forge\s+.https?://' "$PUPPETFILE"; then
          say "Found $PUPPETFILE (librarian-puppet format), installing ..."
          librarian-puppet install --verbose \
            || die "Failed to resolve dependencies from Puppetfile (status $?)"
          rm -rf .librarian
        else
          say "Found $PUPPETFILE (r10k format), installing ..."
          r10k puppetfile install --puppetfile "$PUPPETFILE" --verbose \
            || die "Failed to resolve dependencies from Puppetfile (status $?)"

        break # Skip Puppetfile if Puppetfile_ENVNAME exists
        fi
      fi
    done

    # Generate type hints
    say "Generating types ..."
    puppet generate types --verbose \
      --confdir /etc/puppetlabs/puppet --vardir "$CACHE/puppet.types" \
      --codedir "$WORKDIR" --environment "$ENVNAME" \
      || die "Failed to generate type hints (code $?)"

    # Replace environment with new version
    say "Deploying to $CODEDIR/environments/$ENVNAME ..."
    if [ -d "$CODEDIR/environments/$ENVNAME" ]; then
        chmod -R u=Xrw "$CODEDIR/environments/$ENVNAME"
        rm -r "$CODEDIR/environments/$ENVNAME"
    fi
    mv "$WORKDIR/environments/$ENVNAME" "$CODEDIR/environments/"
    chmod -R u=Xr,g=Xr,o= "$CODEDIR/environments/$ENVNAME"

    # Cleanup
    cd /
    rm -rf "$WORKDIR"
}

undeploy() {
    ENVNAME="$1"
    [[ "$ENVNAME" =~ ^[a-zA-Z0-9_-]+$ ]] || die "Bad environment name"

    if [ -d "$CODEDIR/environments/$ENVNAME" ]; then
        chmod -R u=Xrw "$CODEDIR/environments/$ENVNAME"
        rm -r "$CODEDIR/environments/$ENVNAME"
    fi
}

ca_list() {
    # Must return a tab-separated list with fields:
    #   CERTNAME \t STATE \t FINGERPRINT \t INFO
    # where STATE ist "requested", "signed" or "revoked"
    # and INFO may be any text with additoonal info (e.g. alt names)
    _fixconfdir
    (/opt/puppetlabs/bin/puppetserver ca list --all || exit $?) | awk '
        BEGIN {mode="unknown"; OFS="\t"}
        $2 == "Certificates:" {mode=tolower($1)}
        NF > 2 {
        tail=$4
        for(i=5;i<=NF;i++){
            tail=tail" "$i
        }
        print $1, mode, $2$3, tail
        }
    '
}

ca_sign() {
    test -z "$1" && die "Missing certname"
    _fixconfdir
    puppetserver ca sign --certname "$1" || exit $?
}

ca_revoke() {
    test -z "$1" && die "Missing certname"
    _fixconfdir
    puppetserver ca revoke --certname "$1" || exit $?
}

ca_clean() {
    test -z "$1" && die "Missing certname"
    _fixconfdir
    puppetserver ca clean --certname "$1" && exit
    rc=$?
    test $rc -eq 24 && exit
    exit $rc
}

eyaml_encrypt() {
    eyaml encrypt --pkcs7-public-key "$EYAML_PUBKEY" --stdin --output string <&0
}

eyaml_key() {
    cat "$EYAML_PUBKEY"
}

eyaml_rekey() {
    # We assume old keys to be in $EYAML_KEYDIR subdirectories.
    # (e.g. $EYAML_KEYDIR/old-2023/private_key.pkcs7.pem)

    test -n "$EYAML_ALLKEYS" || die "No keys available"

    STASH="$(mktemp -d)"
    trap 'rm -rf -- "$STASH"' EXIT

    cat <&0 | tr -d "[:space:]" > "$STASH/input"
    egrep --quiet '^ENC\[PKCS7,[a-zA-Z0-9+/]+=*\]$' "$STASH/input" || die "Format error, input not an eyaml secret?"

    for PUBKEY in $EYAML_ALLKEYS; do
        PRIVKEY=$(dirname -- "$PUBKEY")/private_key.pkcs7.pem
        test -r "$PUBKEY" -a -r "$PRIVKEY" || continue

        eyaml decrypt --pkcs7-public-key "$PUBKEY" --pkcs7-private-key "$PRIVKEY" -f "$STASH/input" > "$STASH/plaintext" 2>/dev/null || continue

        # Some errors cause eyaml to silently return its input instead of failing
        cmp --silent "$STASH/input" "$STASH/plaintext" && continue

        # We now have plaintext (plus one additional line break...) in $PLAINTEXT
        if [ "$PUBKEY" -ef "$EYAML_PUBKEY" ]; then
            # Current key worked -> return unchanged input
            cat "$STASH/input"
        else
            # Rotated key worked -> re-encrypt with current key
            head -c -1 "$STASH/plaintext" | eyaml_encrypt # This prints to stdout
        fi
        rm -rf "$STASH"
        exit 0
    done

    die "No key was able to decode input"
}

if [ -n "$SSH_ORIGINAL_COMMAND" ]; then
    main $SSH_ORIGINAL_COMMAND
else
    main "$@"
fi
