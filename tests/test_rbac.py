import pytest
from chucky.auth import RBACPlugin


@pytest.fixture
def rbac():
    rbac = RBACPlugin(get_roles=lambda: ["MANAGER"])
    rbac.register("ADMIN", "MANAGER")
    rbac.register("MANAGER", "USER")
    return rbac


def test_role_implies(rbac):
    assert rbac.user_roles() == set(["MANAGER", "USER"])
    assert rbac.match("USER")
    assert rbac.match("MANAGER")
    assert not rbac.match("ADMIN")
    assert not rbac.match("UNKNOWN")
