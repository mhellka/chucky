import pytest
from chucky.auth import OIDCPlugin
from authlib.jose import JsonWebKey, KeySet


@pytest.fixture
def jwks():
    key = JsonWebKey.generate_key("EC", "P-256")
    jwks = KeySet([key])
    return jwks


def test_redirect_flow(jwks):
    session = {}
    oidc = OIDCPlugin(
        client_id="test",
        client_secret="testsecret",
        get_session=lambda: session,
        authorization_endpoint="https://auth.localhost",
        jwks=jwks.as_dict(),
    )

    redirect = oidc.prepare_login(next_uri="/foo")
    assert oidc.get_state("state")
    assert "/foo" == oidc.get_state("next")
    assert redirect.startswith("https://auth.localhost")
    assert "response_type=code" in redirect
    assert "client_id=test" in redirect
    assert "redirect_uri=http%3A%2F%2F127.0.0.1%2Foidc" in redirect
    assert "scope=openid" in redirect
    assert "state=" + oidc.get_state("state") in redirect
