import subprocess
import chucky.sidecar
from pathlib import Path
import time

ALLCAPS = "health deploy undeploy ca-list ca-sign ca-revoke ca-clean eyaml-encrypt eyaml-key eyaml-rekey".split()


class MockSidecar(chucky.sidecar.Sidecar):
    def __init__(self, *a, **ka):
        super().__init__(*a, **ka)
        self._seed = []
        self._history = []

    def seed(self, cmd, status, stdout, stderr):
        self._seed.append((cmd, status, stdout, stderr))

    def _run(self, command, *a, **ka):
        self._history.append((command, a, ka))
        self.last_run = (command, a, ka)
        assert self._seed, "Unexpected process call"
        cmd, status, stdout, stderr = self._seed.pop(0)
        assert cmd == cmd
        return chucky.sidecar.SidecarResult(
            status,
            [(time.time(), line) for line in stdout.splitlines()],
            [(time.time(), line) for line in stderr.splitlines()],
        )


def test_sidecar():
    sidecar = MockSidecar(command="/bin/echo")
    sidecar.seed(["/bin/echo", "caps"], 0, f"v0 {' '.join(ALLCAPS)}", "")
    assert sidecar.supports("deploy")
    assert not sidecar.supports("make-coffee")

    sidecar.seed(["/bin/echo", "deploy", "myenv"], 0, "", "")
    sidecar.deploy("myenv", Path(__file__).parent)
    assert sidecar.last_run[2]["stdin"].closed

    sidecar.seed(["/bin/echo", "undeploy", "myenv"], 0, "", "")
    sidecar.undeploy("myenv")

    sidecar.seed(["/bin/echo", "ca-list"], 0, "name\trequested\tbar\tbaz", "")
    ca_info = {
        "name": "name",
        "state": "requested",
        "fingerprint": "bar",
        "info": "baz",
        "protected": False,
    }

    assert sidecar.ca_list() == [ca_info]
    assert sidecar.find_cert("name") == ca_info
    # Caching should not trigger more sidecar calls
    assert sidecar.ca_list()
